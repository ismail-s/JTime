export default {
  state: {
    overflowMenuItemSelected: ''
  },
  mutations: {
    overflowMenuItemClicked (state, item) {
      // Object.assign is used to ensure that even the same menu
      // item is clicked twice in a row, any watchers still get
      // triggered.
      state.overflowMenuItemSelected = Object.assign({}, item)
    }
  }
}
