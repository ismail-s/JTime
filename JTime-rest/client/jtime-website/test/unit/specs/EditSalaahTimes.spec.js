import moment from 'moment'
import Vue from 'vue'
import Vuex from 'vuex'
import jsc from 'jsverify'
import chunk from 'lodash.chunk'
import flatten from 'lodash.flatten'
import range from 'lodash.range'
import router from 'src/router'
import EditSalaahTimes from 'src/components/EditSalaahTimes'

describe('EditSalaahTimes.vue', () => {
  function setUpComponent (masjids = [], routerParams = {id: 1, year: 2016, month: 0}) {
    const mockStore = {modules: {
      MasjidsModule: {
        actions: {getSalaahTimesForMonth: sinon.spy()}
      },
      SalaahTimesModule: {
        state: {salaahTimes: {}}
      },
      LoggedInUserModule: {
        state: {loggedInUser: null}
      }}}
    const vm = new Vue({
      el: document.createElement('div'),
      render: (h) => h(EditSalaahTimes),
      store: new Vuex.Store(mockStore),
      router
    })
    return new Promise((resolve, reject) => {
      // We need to explicitly wait for the router.push to complete as
      // EditSalaahTimes route is lazy-loaded ie asynchronously loaded.
      router.push({name: 'edit-salaah-times', params: routerParams}, () => {
        resolve([vm, mockStore])
      }, () => {
        resolve([vm, mockStore])
      })
    })
  }

  it('should initially display a title, help text and some buttons', done => {
    setUpComponent().then(([vm]) => {
      const strings = ['Edit Salaah times', 'Help',
        'Previous month', 'Next month', 'Save changes']
      strings.forEach(text => {
        expect(vm.$el.textContent).to.contain(text)
      })
      done()
    })
  })

  it('dispatches a request to get salaah times when mounted or when the route changes', done => {
    setUpComponent().then(([, mockStore]) => {
      const getSalaahTimesForMonth = mockStore.modules.MasjidsModule.actions.getSalaahTimesForMonth
      expect(getSalaahTimesForMonth).to.have.been.calledOnce
      expect(getSalaahTimesForMonth).to.have.been.calledWith(sinon.match.any, sinon.match(val => {
        return val.type === 'getSalaahTimesForMonth' &&
          val.masjidId === 1 &&
          val.year === 2016 &&
          val.month === 0
      }))
      // Navigate to a new page
      router.push({name: 'edit-salaah-times', params: {id: 1, year: 2016, month: 1}})
      Vue.nextTick(() => {
        expect(getSalaahTimesForMonth).to.have.been.calledTwice
        expect(getSalaahTimesForMonth).to.have.been.calledWith(sinon.match.any, sinon.match(val => {
          return val.type === 'getSalaahTimesForMonth' &&
            val.masjidId === 1 &&
            val.year === 2016 &&
            val.month === 1
        }))
        done()
      })
    })
  })

  describe('buttons to change months', () => {
    const changingMonthTests = [
      {
        title: 'navigate to the next month by clicking the next month button',
        clickOnPrevMonthButton: false,
        expectedYear: 2016,
        expectedMonth: 1},
      {title: 'navigate to the next month when current month is December',
        routerParams: {id: 1, year: 2016, month: 11},
        clickOnPrevMonthButton: false,
        expectedYear: 2017,
        expectedMonth: 0},
      {title: 'navigate to the previous month by clicking the previous month button',
        routerParams: {id: 1, year: 2016, month: 1},
        clickOnPrevMonthButton: true,
        expectedYear: 2016,
        expectedMonth: 0},
      {title: 'navigate to the previous month when current month is January',
        clickOnPrevMonthButton: true,
        expectedYear: 2015,
        expectedMonth: 11}]
    changingMonthTests.forEach(e => {
      it(e.title, done => {
        const setUpPromise = e.routerParams ? setUpComponent([], e.routerParams) : setUpComponent()
        setUpPromise.then(([vm]) => {
          const f = vm.$el.getElementsByTagName('button')
          const prevMonthButton = f[0]
          const nextMonthButton = f[1]
          const buttonToClick = e.clickOnPrevMonthButton ? prevMonthButton : nextMonthButton
          buttonToClick.click()
          expect(router.currentRoute.params).to.have.property('year', e.expectedYear)
          expect(router.currentRoute.params).to.have.property('month', e.expectedMonth)
          done()
        })
      })
    })
  })

  describe('Copy Salaah Times Downwards button', () => {
    function copySalaahTimesDownwards (data, handsontableSpy) {
      const method = EditSalaahTimes.methods.copySalaahTimesDownwards
      const thisArg = {
        data,
        handsontable: {
          setDataAtCell: handsontableSpy
        }
      }
      Object.assign(thisArg, EditSalaahTimes.methods)
      method.call(thisArg)
    }

    function createEmptyTableWithRows (n) {
      return (new Array(n))
        .fill(['blah', '', '', '', ''])
        .map((ls, i) => [i + 1].concat(ls))
    }

    jsc.property('does nothing if table is empty', jsc.nat, n => {
      const data = createEmptyTableWithRows(n)
      const spy = sinon.spy()
      copySalaahTimesDownwards(data, spy)
      expect(spy).to.not.have.been.called
      return true
    })

    jsc.property('does nothing if the whole table is populated', jsc.nearray(jsc.datetime), datetimes1 => {
      let datetimes = chunk(datetimes1.map(d => moment(d).format('HH-mm')), 4)
      if (datetimes[datetimes.length - 1].length !== 4) {
        datetimes.pop()
      }
      // jsverify is generating arrays that are very short. Until this changes,
      // we concat the array to itself a few times to make it longer and make
      // the test a bit more realistic
      datetimes = datetimes.concat(datetimes).concat(datetimes).concat(datetimes)
      const data = datetimes
            .map(ls => ['blah'].concat(ls))
            .map((ls, i) => [i + 1].concat(ls))
      const spy = sinon.spy()
      copySalaahTimesDownwards(data, spy)
      expect(spy).to.not.have.been.called
      return true
    })

    jsc.property('populates a column if there is a time in it, but only below the time', jsc.integer(1, 30), dayOfMonth => {
      for (const col of [2, 3, 4, 5]) {
        const data = createEmptyTableWithRows(31)
        data[dayOfMonth - 1][col] = '12-01'
        const spy = sinon.spy()
        copySalaahTimesDownwards(data, spy)
        expect(spy).to.have.been.calledOnce
        const args = spy.firstCall.args[0]

        // Check that only all the following cells get populated
        expect(args.length).to.equal(31 - dayOfMonth)
        args.forEach(([row, col1, value], i) => {
          expect(row).to.equal(dayOfMonth + i)
          expect(col1).to.equal(col)
          expect(value).to.equal('12-01')
        })
      }
      return true
    })

    it('populates a column correctly when the column had multiple times in it', () => {
      for (const col of [2, 3, 4, 5]) {
        const data = createEmptyTableWithRows(31)
        data[1][col] = '12-01'
        data[9][col] = '13-12'
        data[22][col] = '14-23'
        const spy = sinon.spy()
        copySalaahTimesDownwards(data, spy)
        expect(spy).to.have.been.calledThrice

        // Check that the cells get populated correctly
        const expectedSalaahTimesUpdates = [
          range(2, 9).map(e => [e, col, '12-01']),
          range(10, 22).map(e => [e, col, '13-12']),
          range(23, 31).map(e => [e, col, '14-23'])
        ]
        const actualSalaahTimesUpdates = spy.getCalls().map(e => e.args[0])
        expect(expectedSalaahTimesUpdates).to.deep.equal(actualSalaahTimesUpdates)
      }
    })

    jsc.property('doesn\'t create any new times, only copying existing times', jsc.array(jsc.tuple([jsc.datetime, jsc.integer(1, 31), jsc.integer(2, 5)])), datetimes => {
      const data = createEmptyTableWithRows(31)
      // Populate table randomly with some random times
      for (const [datetime, row, col] of datetimes) {
        data[row - 1][col] = moment(datetime).format('HH-mm')
      }
      const spy = sinon.spy()
      copySalaahTimesDownwards(data, spy)

      // Extract all the salaah times passed to the spy
      const salaahTimes = new Set(flatten(spy.getCalls().map(e => e.args[0].map(f => f[2]))))
      const expectedSalaahTimes = datetimes.map(d => moment(d[0]).format('HH-mm'))
      salaahTimes.forEach(t => expect(expectedSalaahTimes).to.include(t))
      return true
    })

    it('can populate multiple columns in one go', () => {
      const data = createEmptyTableWithRows(31)
      data[1][2] = '12-01'
      data[9][3] = '13-12'
      data[22][4] = '14-23'
      data[21][5] = '15-33'
      const spy = sinon.spy()
      copySalaahTimesDownwards(data, spy)
      expect(spy).to.have.callCount(4)
      const expectedSalaahTimesUpdates = [
        range(2, 31).map(e => [e, 2, '12-01']),
        range(10, 31).map(e => [e, 3, '13-12']),
        range(23, 31).map(e => [e, 4, '14-23']),
        range(22, 31).map(e => [e, 5, '15-33'])
      ]
      const actualSalaahTimesUpdates = spy.getCalls().map(e => e.args[0])
      expect(expectedSalaahTimesUpdates).to.deep.equal(actualSalaahTimesUpdates)
    })

    jsc.property('ignores cells with invalid times', jsc.array(jsc.tuple([jsc.nestring, jsc.integer(1, 31), jsc.integer(2, 5)])), arr => {
      const invalidTimes = arr.filter(([s]) => !moment(s, 'HH-mm', true).isValid())
      const data = createEmptyTableWithRows(31)
      // Populate table randomly with some invalid times
      for (const [invalidTime, row, col] of invalidTimes) {
        data[row - 1][col] = invalidTime
      }
      const spy = sinon.spy()
      copySalaahTimesDownwards(data, spy)
      expect(spy).to.not.have.been.called
      return true
    })
  })

  /* According to https://github.com/handsontable/handsontable/issues/1418#issuecomment-40050828,
     handsontable doesn't render the table contents unless the table is visible. Unless I can come
     up with a way of doing this, I can't test handsontable displaying times or saving times, except
     via e2e tests using eg nightwatch and selenium
  */
})
