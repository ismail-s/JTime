// This is a karma config file. For more details see
//   http://karma-runner.github.io/0.13/config/configuration-file.html
// we are also using it with karma-webpack
//   https://github.com/webpack/karma-webpack

var webpackConfig = require('../../build/webpack.test.conf')

module.exports = function (config) {
  config.set({
    // to run in additional browsers:
    // 1. install corresponding karma launcher
    //    http://karma-runner.github.io/0.13/config/browsers.html
    // 2. add it to the `browsers` array below.
    browsers: ['ChromeHeadlessCustom'],
    // Use Chrome Headless with --no-sandbox option. This is to get
    // Chrome Headless to run in CI. See
    // https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/12144
    // for more information.
    customLaunchers: {
      ChromeHeadlessCustom: {
	base: 'ChromeHeadless',
	displayName: 'Chrome',
	flags: [
	  '--no-sandbox'
	]
      }
    },
    frameworks: ['mocha', 'sinon-chai'],
    reporters: ['spec', 'coverage'],
    files: ['../../node_modules/babel-polyfill/dist/polyfill.js', './index.js'],
    preprocessors: {
      './index.js': ['webpack', 'sourcemap']
    },
    webpack: webpackConfig,
    webpackMiddleware: {
      noInfo: true
    },
    coverageReporter: {
      dir: './coverage',
      reporters: [
        { type: 'lcov', subdir: '.' },
        { type: 'text-summary' }
      ]
    }
  })
}
