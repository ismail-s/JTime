// Top-level build file where you can add configuration options common to all sub-projects/modules.

allprojects {
    repositories {
        jcenter()
        mavenCentral()
        maven {
            url = uri("https://maven.google.com")
        }
        maven {
            url = uri("https://jitpack.io")
        }
    }
}
