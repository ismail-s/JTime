import com.android.build.gradle.internal.dsl.SigningConfig
import com.android.build.gradle.internal.dsl.TestOptions.UnitTestOptions
import de.triplet.gradle.play.PlayPublisherPluginExtension
import org.gradle.kotlin.dsl.closureOf
import org.gradle.kotlin.dsl.kotlin

buildscript {

    extra["kotlinVersion"] = "1.2.0"

    dependencies {
        val kotlinVersion = extra["kotlinVersion"].toString()
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
        classpath("org.jetbrains.kotlin:kotlin-android-extensions:$kotlinVersion")
        classpath("com.google.gms:google-services:3.1.2")
        classpath("com.github.triplet.gradle:play-publisher:1.2.0")
    }
}

plugins {
    id("com.android.application") version "3.0.1"
}

apply {
    plugin("kotlin-android")
    plugin("kotlin-android-extensions")
    plugin("com.github.triplet.play")
}

android {
    compileSdkVersion(26)
    buildToolsVersion("26.0.2")

    defaultConfig {
        minSdkVersion(14)
        targetSdkVersion(23)
        versionCode = 16
        versionName = "1.10.0"
        applicationId = "com.ismail_s.jtime.android"
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }

    sourceSets {
        getByName("main").java.srcDirs("src/main/kotlin")
        getByName("test").java.srcDirs("src/sharedTest/kotlin")
        getByName("androidTest").java.srcDirs("src/sharedTest/kotlin")
    }

    signingConfigs {
        create("release") {
            storeFile = file("android.jks")
            keyAlias = System.getenv("ANDROID_RELEASE_KEY_ALIAS")
            storePassword = System.getenv("ANDROID_RELEASE_STORE_PASSWORD")
            keyPassword = System.getenv("ANDROID_RELEASE_KEY_PASSWORD")
        }
    }

    buildTypes {
        getByName("release") {
            signingConfig = signingConfigs.getByName("release")
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android.txt"))
            proguardFiles(*fileTree(mapOf(
                    "dir" to "./proguard-config",
                    "include" to arrayOf("*.pro")))
                    .toList().toTypedArray())
        }
    }

    testOptions {
        unitTests(closureOf<UnitTestOptions> {
            isIncludeAndroidResources = true
        })
    }
}

configure<PlayPublisherPluginExtension> {
    setTrack("beta")
    jsonFile = file("google-play-publisher-keys.json")
}

dependencies {

    implementation("com.mikepenz:materialdrawer:5.1.6@aar") {
        isTransitive = true
    }

    // Espresso
    androidTestImplementation("com.android.support.test.espresso:espresso-core:3.0.1")
    androidTestImplementation("com.android.support.test:runner:1.0.1")
    androidTestImplementation("com.android.support.test:rules:1.0.1")

    // Robolectric
    testImplementation("junit:junit:4.12")
    testImplementation("org.robolectric:robolectric:3.6.1")
    testImplementation("org.robolectric:shadows-supportv4:3.6.1")

    //Android support library
    implementation("com.android.support:support-v4:26.0.1")
    implementation("com.android.support:support-v13:26.0.1")
    androidTestImplementation("com.android.support:support-annotations:26.0.1")

    //Google play services
    arrayOf("auth", "maps", "location", "places").forEach {
        implementation("com.google.android.gms:play-services-$it:11.6.2")
    }

    //Kotlin
    implementation(kotlin("stdlib", "${extra["kotlinVersion"]}"))
    testImplementation(kotlin("test-junit", "${extra["kotlinVersion"]}"))

    //Okhttp mockwebserver
    val mockWebServerDep = "com.squareup.okhttp3:mockwebserver:3.2.0"
    androidTestImplementation(mockWebServerDep)
    testImplementation(mockWebServerDep)


    //Recyclerview & Cardview
    implementation("com.android.support:recyclerview-v7:26.0.1")
    implementation("com.android.support:cardview-v7:26.0.1")

    // FlexboxLayout
    implementation("com.google.android:flexbox:0.2.5")

    //Fuel (networking library)
    implementation("com.github.kittinunf.fuel:fuel-android:1.3.1")

    val findBugsDep = "com.google.code.findbugs:jsr305:3.0.1"
    implementation(findBugsDep)
    androidTestImplementation(findBugsDep)

    //Kotlin promises
    arrayOf("core", "android").forEach {
        implementation("nl.komponents.kovenant:kovenant-$it:3.3.0")
    }

    //Kotlin anko
    arrayOf("sdk15", "support-v4", "cardview-v7").forEach {
        implementation("org.jetbrains.anko:anko-$it:0.9")
    }

    //Betterpickers (datepicker)
    implementation("com.code-troopers.betterpickers:library:3.1.0")

    //Material Dialogs
    implementation("com.afollestad.material-dialogs:core:0.9.4.7")

    //AppIntro
    implementation("com.github.apl-devs:appintro:v4.2.2")
}

apply {
    plugin("com.google.gms.google-services")
}
