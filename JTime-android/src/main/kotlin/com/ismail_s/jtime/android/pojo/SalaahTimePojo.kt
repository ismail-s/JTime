package com.ismail_s.jtime.android.pojo

import android.location.Location
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import java.util.*

/**
 * Reresents a single salaah time for a given masjid.
 */
class SalaahTimePojo(val masjidId: Int, val type: SalaahType, val datetime: GregorianCalendar) : Parcelable {
    var id: Int? = null

    var masjidName: String? = null

    var masjidLoc: Location? = null

    constructor(masjidId: Int, masjidName: String, masjidLoc: Location, type: SalaahType,
            datetime: GregorianCalendar) : this(masjidId, type, datetime) {
        this.masjidName = masjidName
        this.masjidLoc = masjidLoc
    }

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<SalaahTimePojo> = object : Parcelable.Creator<SalaahTimePojo> {
            override fun createFromParcel(source: Parcel) = SalaahTimePojo(source)
            override fun newArray(size: Int) = arrayOfNulls<SalaahTimePojo>(size)
        }
    }

    private constructor(source: Parcel) : this(source.readInt(), SalaahType.values()[source.readInt()], source.readSerializable() as GregorianCalendar) {
        if (source.dataAvail() > 0) {
            this.masjidName = source.readString()
            this.masjidLoc = source.readParcelable(null)
        }
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeInt(masjidId)
        dest?.writeInt(type.ordinal)
        dest?.writeSerializable(datetime)
        if (masjidName != null && masjidLoc != null) {
            dest?.writeString(masjidName)
            dest?.writeParcelable(masjidLoc, 0)
        }
    }
}