package com.ismail_s.jtime.android.pojo

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import com.ismail_s.jtime.android.R
import java.util.Calendar.DAY_OF_WEEK
import java.util.Calendar.FRIDAY
import java.util.GregorianCalendar

/**
 * Represents a masjid with some salaah times for a particular day. This class will probably be
 * changed in future as the abstraction it represents does not make complete sense.
 */
class MasjidPojo : Parcelable {
    var name: String? = null
    var id: Int? = null
    var address: String = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var fajrTime: GregorianCalendar? = null
    var zoharTime: GregorianCalendar? = null
    var asrTime: GregorianCalendar? = null
    var magribTime: GregorianCalendar? = null
    var eshaTime: GregorianCalendar? = null

    constructor(name: String, fajrTime: GregorianCalendar, zoharTime: GregorianCalendar, asrTime: GregorianCalendar, magribTime: GregorianCalendar, eshaTime: GregorianCalendar) {
        this.name = name
        this.fajrTime = fajrTime
        this.zoharTime = zoharTime
        this.asrTime = asrTime
        this.magribTime = magribTime
        this.eshaTime = eshaTime
    }

    constructor(name: String) {
        this.name = name
    }

    constructor(name: String, id: Int, address: String, latitude: Double, longitude: Double) {
        this.name = name
        this.id = id
        this.address = address
        this.latitude = latitude
        this.longitude = longitude
    }

    constructor()

    val times: Array<GregorianCalendar>
        get() = arrayOf(fajrTime!!, zoharTime!!, asrTime!!, magribTime!!, eshaTime!!)

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        id = parcel.readInt().let { if (it == -1) null else it }
        address = parcel.readString()
        latitude = parcel.readDouble()
        longitude = parcel.readDouble()
        val getNextTime: () -> GregorianCalendar? = {
            val num = parcel.readLong()
            if (num == -1L) null else GregorianCalendar().apply { timeInMillis = num }
        }
        fajrTime = getNextTime()
        zoharTime = getNextTime()
        asrTime = getNextTime()
        magribTime = getNextTime()
        eshaTime = getNextTime()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeInt(id ?: -1)
        parcel.writeString(address)
        parcel.writeDouble(latitude)
        parcel.writeDouble(longitude)
        parcel.writeLong(fajrTime?.timeInMillis ?: -1)
        parcel.writeLong(zoharTime?.timeInMillis ?: -1)
        parcel.writeLong(asrTime?.timeInMillis ?: -1)
        parcel.writeLong(magribTime?.timeInMillis ?: -1)
        parcel.writeLong(eshaTime?.timeInMillis ?: -1)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<MasjidPojo> {
        override fun createFromParcel(parcel: Parcel) = MasjidPojo(parcel)

        override fun newArray(size: Int): Array<MasjidPojo?> = arrayOfNulls(size)
    }
}

/**
 * Salaah types that are used both on the rest server and this app.
 *
 * @property resId resource id for the string representation of this salaah type
 * @property apiRef the code used to represent this salaah type when talking to the rest server
 */
enum class SalaahType(private val resId: Int, val apiRef: Char) {
    FAJR(R.string.fajr, 'f'), ZOHAR(R.string.zohar, 'z'),
    ASR(R.string.asr, 'a'), MAGRIB(R.string.magrib, 'm'), ESHA(R.string.esha, 'e');

    /**
     * Return a localised string representation of this salaah type.
     */
    fun toString(context: Context, date: GregorianCalendar): String {
        // If it's Friday, then Zohar is really Jumma
        if (date.get(DAY_OF_WEEK) == FRIDAY && this.apiRef == 'z') {
            return context.getString(R.string.jumma)
        }
        return context.getString(resId)
    }
}

/**
 * Convert the code representing a salaah type to a [SalaahType]. If the code is invalid,
 * then [SalaahType.ESHA] is returned.
 */
fun charToSalaahType(c: Char): SalaahType = when(c) {
    'f' -> SalaahType.FAJR
    'z' -> SalaahType.ZOHAR
    'a' -> SalaahType.ASR
    'm' -> SalaahType.MAGRIB
    else -> SalaahType.ESHA
}
