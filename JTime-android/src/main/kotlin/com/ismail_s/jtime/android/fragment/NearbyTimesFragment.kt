package com.ismail_s.jtime.android.fragment

import android.location.Location
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableLayout
import android.widget.TextView
import com.ismail_s.jtime.android.CalendarFormatter.formatCalendarAsTime
import com.ismail_s.jtime.android.CalendarFormatter.formatCalendarAsTodayOrDate
import com.ismail_s.jtime.android.MasjidDetailsDialog.showMasjidDetailsDialog
import com.ismail_s.jtime.android.MainActivity
import com.ismail_s.jtime.android.R
import com.ismail_s.jtime.android.RestClient
import com.ismail_s.jtime.android.pojo.MasjidPojo
import com.ismail_s.jtime.android.pojo.SalaahTimePojo
import com.ismail_s.jtime.android.pojo.SalaahType
import kotlinx.android.synthetic.main.fragment_nearby_times.*
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.ui.failUi
import nl.komponents.kovenant.ui.successUi
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.act
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.toast
import java.util.*

/**
 * Displays nearby salaah times for today, for a given salaah type.
 */
class NearbyTimesFragment : BaseFragment() {
    lateinit private var salaahType: SalaahType
    private var date: GregorianCalendar = GregorianCalendar()
    private var location: Location? = null
    private var locationName: String? = null
    private var times: List<SalaahTimePojo> = emptyList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        location = arguments.getParcelable(LOCATION)
        locationName = arguments.getString(LOCATION_NAME)
        date = arguments.getSerializable(DATE) as GregorianCalendar? ?: GregorianCalendar()
        salaahType = arguments.getSerializable(SALAAH_TYPE) as SalaahType
        times = (arguments.getSerializable(TIMES) as? List<*>)?.filterIsInstance<SalaahTimePojo>() ?: emptyList()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_nearby_times, container, false)
        val table = rootView.find<TableLayout>(R.id.salaah_times_table)
        ViewCompat.setTransitionName(table, "homeToNearbyTimesFragment-${salaahType.name}")
        val salaahNameLabel = rootView.find<TextView>(R.id.label_salaah_name)
        val locStr = locationName?.let { " for $it" } ?: ""
        salaahNameLabel.text = getString(R.string.nearby_times_title_text, salaahType.toString(ctx, date), formatCalendarAsTodayOrDate(date), locStr)
        if (times.isNotEmpty()) {
            displayInUi(times, table)
        } else {
            val promise = location?.let { Promise.of(it) } ?: MainActivity.location
            promise successUi {
                getTimesAndDisplayInUi(it)
            } failUi {
                debug("Failed to get current location")
                toast(getString(R.string.get_location_for_nearby_masjids_failure_toast))
            }
        }
        return rootView
    }

    override fun onLocationChanged(loc: Location) {
        if (location == null) getTimesAndDisplayInUi(loc)
    }

    private fun getTimesAndDisplayInUi(loc: Location) {
        cancelPromiseOnFragmentDestroy {
            RestClient.getTimesForNearbyMasjids(loc.latitude, loc.longitude, date, salaahType)
                    .successUi {
                        displayInUi(it)
                    } failUi {
                        ifAttachedToAct {
                            debug("Failed to get nearby times from masjid")
                            toast(getString(R.string.get_masjid_times_failure_toast, it.message))
                        }
            }
        }
    }

    private fun displayInUi(timesToDisplay: List<SalaahTimePojo>, table: TableLayout = salaah_times_table) {
        table.removeAllViews()
        val tSize = 18f
        if (timesToDisplay.isEmpty()) {
            table.addView(with(AnkoContext.create(act, table)) {
                tableRow {
                    textView(getString(R.string.no_salaah_times_nearby_masjids_toast)) {
                        textSize = tSize
                    }
                }
            })
            return
        }
        timesToDisplay.sortedBy { it.datetime.timeInMillis }.forEachIndexed { i, t ->
            table.addView(with(AnkoContext.create(act, table)) {
                tableRow {
                    if (i % 2 == 0) backgroundColor = ContextCompat.getColor(ctx, R.color.md_grey_700)
                    textView(t.masjidName) {
                        textSize = tSize
                    }
                    textView(formatCalendarAsTime(t.datetime)) {
                        textSize = tSize
                    }
                    onClick {
                        val masjidLoc = t.masjidLoc
                        if (masjidLoc != null) {
                            val masjidPojo = MasjidPojo(t.masjidName ?: "", t.masjidId, "", masjidLoc.latitude, masjidLoc.longitude)
                            showMasjidDetailsDialog(act, masjidPojo)
                        }
                    }
                }
            })
        }
    }

    companion object {
        private val SALAAH_TYPE = "salaahType"
        private val LOCATION = "NearbyTimesFragment.LOCATION"
        private val LOCATION_NAME = "NearbyTimesFragment.LOCATION_NAME"
        private val DATE = "NearbyTimesFragment.DATE"
        private val TIMES = "NearbyTimesFragment.TIMES"

        fun newInstance(salaahType: SalaahType, date: GregorianCalendar,
                        location: Location?, locationName: String?, times: List<SalaahTimePojo>): NearbyTimesFragment {
            val fragment = NearbyTimesFragment()
            val args = Bundle()
            args.putSerializable(TIMES, ArrayList(times))
            args.putSerializable(SALAAH_TYPE, salaahType)
            args.putSerializable(DATE, date)
            args.putParcelable(LOCATION, location)
            args.putString(LOCATION_NAME, locationName)
            fragment.arguments = args
            return fragment
        }
    }
}
