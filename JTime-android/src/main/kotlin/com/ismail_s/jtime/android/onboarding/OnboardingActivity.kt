package com.ismail_s.jtime.android.onboarding

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import com.github.paolorotolo.appintro.AppIntro2
import com.github.paolorotolo.appintro.AppIntro2Fragment
import com.ismail_s.jtime.android.MainActivity
import com.ismail_s.jtime.android.R
import com.ismail_s.jtime.android.SharedPreferencesWrapper
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask

/**
 * Onboarding activity shown when app is first run.
 */
class OnboardingActivity: AppIntro2() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addSlide(AppIntro2Fragment.newInstance(getString(R.string.app_name), getString(R.string.onboarding_description_slide_1), R.drawable.ic_icon, resources.getColor(R.color.icon_green)))
        val dark = resources.getColor(R.color.md_grey_850)
        addSlide(AppIntroHtmlDescriptionFragment.newInstance(getString(R.string.onboarding_title_how_it_works), getString(R.string.onboarding_description_slide_2), R.drawable.ic_phone_and_laptop, dark))
        addSlide(AppIntro2Fragment.newInstance(getString(R.string.onboarding_title_how_it_works), getString(R.string.onboarding_description_slide_3), R.drawable.ic_salaah_times_table, dark))
        addSlide(AppIntro2Fragment.newInstance(getString(R.string.onboarding_title_how_it_works), getString(R.string.onboarding_description_slide_4), R.drawable.ic_salaah_times_table_edit, dark))
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            addSlide(AppIntroHtmlDescriptionFragment.newInstance(getString(R.string.onboarding_title_one_last_thing), getString(R.string.onboarding_description_slide_5), R.drawable.ic_location_on_white, dark))
            askForPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 5)
            setSwipeLock(false)
        }
        setColorTransitionsEnabled(true)
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        finishAndGoToMainActivity()
    }

    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        finishAndGoToMainActivity()
    }

    private fun finishAndGoToMainActivity() {
        SharedPreferencesWrapper(this).firstTimeUsingApp = false
        startActivity(intentFor<MainActivity>().newTask().clearTask())
    }
}
