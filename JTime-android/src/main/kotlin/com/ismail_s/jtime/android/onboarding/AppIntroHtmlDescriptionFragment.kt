package com.ismail_s.jtime.android.onboarding

import android.os.Bundle
import android.support.annotation.ColorInt
import android.support.annotation.DrawableRes
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.github.paolorotolo.appintro.AppIntroBaseFragment
import com.ismail_s.jtime.android.R
import org.jetbrains.anko.find

/**
 * Like [AppIntroBaseFragment] but allows the description to contain html and clickable links
 */
class AppIntroHtmlDescriptionFragment : AppIntroBaseFragment() {

    override fun getLayoutId() = R.layout.fragment_intro

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        val descriptionView = view?.find<TextView>(R.id.description)
        descriptionView?.text = Html.fromHtml(descriptionView?.text.toString())
        descriptionView?.movementMethod = LinkMovementMethod.getInstance()
        return view
    }

    companion object {

        /**
         * See [com.github.paolorotolo.appintro.AppIntroFragment.newInstance]
         */
        fun newInstance(title: String, description: String, @DrawableRes imageDrawable: Int, @ColorInt bgColor: Int): AppIntroHtmlDescriptionFragment {
            val slide = AppIntroHtmlDescriptionFragment()
            val args = Bundle()
            args.putString(ARG_TITLE, title)
            args.putString(ARG_TITLE_TYPEFACE, null)
            args.putString(ARG_DESC, description)
            args.putString(ARG_DESC_TYPEFACE, null)
            args.putInt(ARG_DRAWABLE, imageDrawable)
            args.putInt(ARG_BG_COLOR, bgColor)
            args.putInt(ARG_TITLE_COLOR, 0)
            args.putInt(ARG_DESC_COLOR, 0)
            slide.arguments = args

            return slide
        }
    }
}
