package com.ismail_s.jtime.android

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.annotation.NonNull
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.transition.ChangeBounds
import android.transition.ChangeTransform
import android.transition.Fade
import android.transition.TransitionSet
import android.view.Menu
import android.view.View
import android.widget.FrameLayout
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.ismail_s.jtime.android.fragment.*
import com.ismail_s.jtime.android.onboarding.OnboardingActivity
import com.ismail_s.jtime.android.pojo.MasjidPojo
import com.ismail_s.jtime.android.pojo.SalaahTimePojo
import com.ismail_s.jtime.android.pojo.SalaahType
import com.mikepenz.materialdrawer.AccountHeader
import com.mikepenz.materialdrawer.AccountHeaderBuilder
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.ProfileDrawerItem
import nl.komponents.kovenant.android.startKovenant
import nl.komponents.kovenant.android.stopKovenant
import nl.komponents.kovenant.deferred
import nl.komponents.kovenant.ui.failUi
import nl.komponents.kovenant.ui.successUi
import org.jetbrains.anko.*
import java.util.*

/**
 * This activity holds all the fragments that make up the app, and manages the nav drawers and
 * Google Play Services (ie login with google, location).
 */
class MainActivity : AppCompatActivity(), AnkoLogger, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, ActivityCompat.OnRequestPermissionsResultCallback {
    var drawer: Drawer? = null
    private lateinit var header: AccountHeader
    lateinit var toolbar: Toolbar
    private val locationRequest: LocationRequest by lazy {
        val locRequest = LocationRequest()
        locRequest.interval = 20000
        locRequest.fastestInterval = 10000
        locRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        locRequest
    }
    /**
    * Login status is 0 for don't know, 1 for logged in and 2 for logged out
    */
    private var loginStatus = 0
    val currentFragment: BaseFragment?
        get() = supportFragmentManager.findFragmentById(R.id.fragment_container) as BaseFragment?
    private val LOGIN_DRAWER_ITEM_IDENTIFIER: Long = 546
    private val LOGOUT_DRAWER_ITEM_IDENTIFIER: Long = 232
    private val ADD_MASJID_DRAWER_ITEM_IDENTIFIER: Long = 785
    val HELP_DRAWER_ITEM_IDENTIFIER: Long = 365

    private val TOOLBAR_TITLE = "toolbar_title"
    private val LOGIN_STATUS = "login_status"

    private val locationListener = LocationListener {
        /*Location should be updated unless the new location is less than
          50 metres from the last location we had. 50 is an arbitrarily
          chosen small-but-not-too-small number.*/
        val weShouldUpdateTheLocation = if (Companion.location.isSuccess())
                Companion.location.get().distanceTo(it) >= 50
            else true
        if (weShouldUpdateTheLocation) {
            Companion.locationDeferred = deferred()
            Companion.locationDeferred resolve it
            currentFragment?.onLocationChanged(it)
        }
    }

    private val logoutDrawerItem: PrimaryDrawerItem
        get() = PrimaryDrawerItem()
                .withName(getString(R.string.drawer_item_logout))
                .withIdentifier(LOGOUT_DRAWER_ITEM_IDENTIFIER)
                .withOnDrawerItemClickListener { _, _, _ ->
                    RestClient.logout() successUi {
                        loginStatus = 2
                        toast(getString(R.string.logout_success_toast))
                        //Remove logout button, add login button to nav drawer
                        header.removeProfile(0)
                        drawer?.removeItem(LOGOUT_DRAWER_ITEM_IDENTIFIER)
                        drawer?.addItemAtPosition(loginDrawerItem, 0)
                        //remove addMasjidDrawerItem
                        drawer?.removeItem(ADD_MASJID_DRAWER_ITEM_IDENTIFIER)
                        currentFragment?.onLogout()
                    } failUi {
                        toast(getString(R.string.logout_failure_toast, it.message))
                    }
                    true
                }

    private val loginDrawerItem: PrimaryDrawerItem
        get() = PrimaryDrawerItem()
                .withName(getString(R.string.drawer_item_login))
                .withIdentifier(LOGIN_DRAWER_ITEM_IDENTIFIER)
                .withOnDrawerItemClickListener { _, _, _ ->
                    val signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient)
                   startActivityForResult(signInIntent, RC_SIGN_IN)
                    true
                }

    private val addMasjidDrawerItem: PrimaryDrawerItem
        get() = PrimaryDrawerItem()
                .withName(getString(R.string.drawer_item_add_masjid))
                .withIdentifier(ADD_MASJID_DRAWER_ITEM_IDENTIFIER)
                .withOnDrawerItemClickListener { _, _, _ ->
                    switchToAddMasjidFragment()
                    true
                }

    /**
     * Called when Sign in with Google fails
     */
    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        toast(getString(R.string.login_failure_toast, connectionResult.toString()))
    }

    /**
     * Called when we connect to Google Play Services. In this method, we sort out stuff relating
     * to getting the users location.
     */
    override fun onConnected(connectionHint: Bundle?) {
        //Check if location permission has been granted
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_RESULT_CODE)
            return
        }
        if (!Companion.location.isDone()) {
            val loc = LocationServices.FusedLocationApi.getLastLocation(googleApiClient)
            if (loc == null) {
                Companion.locationDeferred reject Exception(getString(R.string.no_location_exception))
            } else {
                Companion.locationDeferred resolve loc
            }
        }
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
                .setResultCallback {
                    when(it.status.statusCode) {
                        LocationSettingsStatusCodes.SUCCESS ->{
                            //We can request the location now
                            startLocationUpdates()
                        }
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                            //Show dialog prompting user to change location settings
                            it.status.startResolutionForResult(this, RC_CHECK_SETTINGS)
                            longToast("Please change your location settings in order to see nearby salaah times.")
                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                            //Show a toast saying we can't get the location at all
                            toast(getString(R.string.no_location_exception))
                        }
                    }
                }
    }

    private fun startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                googleApiClient?.isConnected == true) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest,
                locationListener)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<out String>, @NonNull grantResults: IntArray) {
        if (requestCode == LOCATION_PERMISSION_RESULT_CODE) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onConnected(null)
            } else if (!Companion.location.isDone()) {
                Companion.locationDeferred reject Exception(getString(R.string.no_location_exception))
            }
        }
    }

    override fun onConnectionSuspended(cause: Int) {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        startKovenant()
        RestClient.initialise(applicationContext)

        if (SharedPreferencesWrapper(this).firstTimeUsingApp) {
            startActivity(intentFor<OnboardingActivity>().newTask().clearTask())
            return
        }

        toolbar = find(R.id.toolbar)
        toolbar.title = savedInstanceState?.getCharSequence(TOOLBAR_TITLE, "") ?: ""
        setSupportActionBar(toolbar)
        setUpGoogleApiClient()
        setUpNavDrawer(savedInstanceState)

        val onLoggedOut = {
            loginStatus = 2
            //Set button to be login, create drawer
            drawer?.addItemAtPosition(loginDrawerItem, 0)
        }
        val onLoggedIn = {
            loginStatus = 1
            //Set button to be logout, create drawer
            drawer?.addItemAtPosition(logoutDrawerItem, 0)
            val email: String = SharedPreferencesWrapper(this@MainActivity).email
            header.addProfile(ProfileDrawerItem().withEmail(email), 0)
            //add addMasjidDrawerItem
            drawer?.addItem(addMasjidDrawerItem)
        }
        val loggedInStatus = savedInstanceState?.getInt(LOGIN_STATUS, 0) ?: 0
        when (loggedInStatus) {
            1 -> onLoggedIn()
            2 -> onLoggedOut()
            else -> RestClient.areWeStillSignedInOnServer()
                .successUi {onLoggedIn()}
                .failUi {onLoggedOut()}
        }

        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById<FrameLayout>(R.id.fragment_container) != null) {
            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return
            }
            switchToHomeFragment()
        }
    }

    private fun setUpGoogleApiClient() {
        if (googleApiClient != null)
            return
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken("654477471044-i8156m316nreihgdqoicsh0gktgqjaua.apps.googleusercontent.com")
                .build()
        googleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
    }

    private fun setUpNavDrawer(savedInstance: Bundle?) {
        val drawerListener = object: Drawer.OnDrawerListener {
            override fun onDrawerOpened(drawerView: View) {
                currentFragment?.onDrawerOpened(drawerView)
            }

            override fun onDrawerClosed(drawerView: View) {
                currentFragment?.onDrawerClosed(drawerView)
            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {}
        }
        header = AccountHeaderBuilder().withActivity(this)
                .withProfileImagesVisible(false).withCompactStyle(true)
                .build()
        drawer = DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(header)
                .withToolbar(toolbar)
                .withActionBarDrawerToggle(true)
                .withShowDrawerOnFirstLaunch(true)
                .withOnDrawerListener(drawerListener)
                .withSavedInstance(savedInstance)
                .addDrawerItems(PrimaryDrawerItem()
                        .withName(getString(R.string.drawer_item_home))
                        .withOnDrawerItemClickListener { _, _, _ ->
                            switchToHomeFragment()
                            true
                        }, PrimaryDrawerItem()
                        .withName(getString(R.string.drawer_item_all_masjids))
                        .withOnDrawerItemClickListener { _, _, _ ->
                            switchToAllMasjidsFragment()
                            true
                        }, PrimaryDrawerItem()
                        .withName(getString(R.string.drawer_item_help))
                        .withIdentifier(HELP_DRAWER_ITEM_IDENTIFIER)
                        .withOnDrawerItemClickListener {_, _, _ ->
                            switchToHelpFragment()
                            true
                        })
                .build()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        currentFragment?.onCreateOptionsMenu(menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onStart() {
        googleApiClient?.connect()
        super.onStart()
    }

    override fun onStop() {
        googleApiClient?.disconnect()
        super.onStop()
    }

    override fun onPause() {
        super.onPause()
        if (googleApiClient?.isConnected == true)
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, locationListener)
    }

    override fun onResume() {
        super.onResume()
        if (googleApiClient?.isConnected == true)
            startLocationUpdates()
    }

    override fun onDestroy() {
        stopKovenant()
        googleApiClient = null
        super.onDestroy()
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)
        //Save current title
        savedInstanceState.putCharSequence(TOOLBAR_TITLE, toolbar.title)
        //Save logged in state
        savedInstanceState.putInt(LOGIN_STATUS, loginStatus)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            RC_SIGN_IN -> {
                val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
                if (result.isSuccess) {
                    loginStatus = 1
                    val acct = result.signInAccount as GoogleSignInAccount
                    RestClient.login(acct.idToken!!, acct.email!!) successUi {
                        header.addProfile(ProfileDrawerItem().withEmail(acct.email), 0)
                        //Remove login button, add logout button to nav drawer
                        drawer?.removeItem(LOGIN_DRAWER_ITEM_IDENTIFIER)
                        drawer?.addItemAtPosition(logoutDrawerItem, 0)
                        //add addMasjidDrawerItem
                        drawer?.addItem(addMasjidDrawerItem)
                        toast(getString(R.string.login_success_toast))
                        currentFragment?.onLogin()
                    } failUi {
                        toast(getString(R.string.login_failure_toast, it.message))
                    }
                }
            }
            RC_CHECK_SETTINGS -> {
                when (resultCode) {
                    RESULT_OK -> {
                        //Location settings were successfully changed
                        toast("Location settings changed. Trying to get your location.")
                        startLocationUpdates()
                    }
                    RESULT_CANCELED -> {
                        //Location settings were not changed
                        toast("Location settings weren't changed. Some features in the app won't work fully.")
                    }
                }
            }
			else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    fun switchToMasjidsFragment(masjidPojo: MasjidPojo) {
        val fragment = MasjidsFragment.newInstance(masjidPojo)
        switchToFragment(fragment, R.string.fragment_title_masjid_times)
    }

    fun switchToAllMasjidsFragment() = switchToFragment(AllMasjidsFragment(),
            R.string.fragment_title_all_masjids)

    fun switchToHomeFragment() = switchToFragment(HomeFragment(),
            R.string.fragment_title_home)

    fun switchToAddMasjidFragment() = switchToFragment(AddMasjidFragment(),
            R.string.fragment_title_add_masjid)

    fun switchToHelpFragment() = switchToFragment(HelpFragment(), R.string.fragment_title_help)

    fun switchToChangeMasjidTimesFragment(masjidId: Int, masjidName: String, date: GregorianCalendar) {
        val fragment = ChangeMasjidTimesFragment.newInstance(masjidId, masjidName, date)
        switchToFragment(fragment, R.string.fragment_title_change_salaah_times)
    }

    fun switchToNearbyTimesFragment(sharedElement: View, salaahType: SalaahType, date: GregorianCalendar, location: Location?, locationName: String?, times: List<SalaahTimePojo>) {
        val fragment = NearbyTimesFragment.newInstance(salaahType, date, location, locationName, times)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            currentFragment?.exitTransition = Fade()
            fragment.enterTransition = Fade()
            fragment.sharedElementEnterTransition = HomeToNearbyTimesTransition()
            fragment.sharedElementReturnTransition = HomeToNearbyTimesTransition()
        }
        debug("Switching to fragment $fragment")
        supportFragmentManager.beginTransaction()
                .sortOutBackStack(fragment)
                .addSharedElement(sharedElement, "homeToNearbyTimesFragment-${salaahType.name}")
                .replace(R.id.fragment_container, fragment).commit()
        drawer?.closeDrawer()
        toolbar.title = getString(R.string.fragment_title_nearby_times)
        Handler().post { invalidateOptionsMenu() }
    }

    private fun switchToFragment(fragment: BaseFragment, title: Int) {
        debug("Switching to fragment $fragment")
        supportFragmentManager.beginTransaction()
                .sortOutBackStack(fragment)
                .replace(R.id.fragment_container, fragment).commit()
        drawer?.closeDrawer()
        toolbar.title = getString(title)
        Handler().post { invalidateOptionsMenu() }
    }

    override fun onBackPressed() {
        // If nav drawer is open, close it
        if (drawer?.isDrawerOpen == true) {
            drawer?.closeDrawer()
            return
        }
        super.onBackPressed()
        invalidateOptionsMenu()
        // Update toolbar title
        toolbar.title = getString(when (currentFragment) {
            is HomeFragment -> R.string.fragment_title_home
            is AllMasjidsFragment -> R.string.fragment_title_all_masjids
            is MasjidsFragment -> R.string.fragment_title_masjid_times
            else -> return
        })
    }

    private fun FragmentTransaction.sortOutBackStack(to: BaseFragment): FragmentTransaction {
        val from = currentFragment ?: return this
        val fm = supportFragmentManager
        val clearBackStack = { repeat(fm.backStackEntryCount, { _ -> fm.popBackStackImmediate() }) }
        when (from) {
            is HomeFragment -> when (to) {
                is NearbyTimesFragment -> addToBackStack(null)
                else -> clearBackStack()
            }
            is AllMasjidsFragment -> when (to) {
                is MasjidsFragment -> addToBackStack(null)
                else -> clearBackStack()
            }
            is MasjidsFragment -> when (to) {
                is ChangeMasjidTimesFragment -> addToBackStack(null)
                else -> clearBackStack()
            }
            else -> clearBackStack()
        }
        return this
    }

    companion object {
        private val RC_SIGN_IN = 9001
        private val RC_CHECK_SETTINGS = 9002
        private val LOCATION_PERMISSION_RESULT_CODE = 11
        var googleApiClient: GoogleApiClient? = null
        // location is within the companion object to ensure that only one such variable exists,
        // which helps with mocking out the location during testing.
        var locationDeferred = deferred<Location, Exception>()
        val location
            get() = Companion.locationDeferred.promise
    }
}

@SuppressLint("NewApi")
class HomeToNearbyTimesTransition : TransitionSet() {
    init {
        ordering = ORDERING_TOGETHER
        duration = 200
        addTransition(ChangeBounds()).addTransition(ChangeTransform())
    }
}
