package com.ismail_s.jtime.android.MasjidDetailsDialog

import android.app.Activity
import android.content.Intent
import android.net.Uri
import com.afollestad.materialdialogs.MaterialDialog
import com.ismail_s.jtime.android.RestClient
import com.ismail_s.jtime.android.pojo.MasjidPojo
import nl.komponents.kovenant.ui.successUi

/**
 * Show a dialog with details for a given masjid.
 */
fun showMasjidDetailsDialog(activity: Activity, masjidPojo: MasjidPojo) {
    val address = if (masjidPojo.address == "") null else masjidPojo.address
    val content = if (address != null) "Address: $address" else ""
    val dialog = MaterialDialog.Builder(activity)
            .title(masjidPojo.name ?: "").content(content)
            .neutralText("Open in maps app").onNeutral { _, _ ->
        val latAndLng = "${masjidPojo.latitude},${masjidPojo.longitude}"
        val label = if (masjidPojo.name != null && address != null) {
            "${masjidPojo.name}, ${masjidPojo.address}"
        }
        else {
            address ?: if (masjidPojo.name !=  null) masjidPojo.name else ""
        }
        val uri = Uri.parse("geo:$latAndLng?q=$latAndLng($label)")
        activity.startActivity(Intent(Intent.ACTION_VIEW, uri))
    }.show()
    if (address == null) {
        RestClient.getMasjids() successUi {
            if (dialog.isShowing) {
                val newAddress = it.find { it.id == masjidPojo.id }?.address
                val newContent = if (newAddress != null && newAddress != "") "Address: $newAddress" else ""
                dialog.setContent(newContent)
            }
        }
    }
}
