package com.ismail_s.jtime.android

import com.ismail_s.jtime.android.MockWebServer.createMockWebServerAndConnectToRestClient
import com.ismail_s.jtime.android.onboarding.OnboardingActivity
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.shadows.ShadowApplication

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [21])
class MainActivityLaunchesOnboardingActivityTest {
    private lateinit var activity: MainActivity

    @Before
    fun setupActivity() {
        createMockWebServerAndConnectToRestClient()
        activity = Robolectric.setupActivity(MainActivity::class.java)
    }

    @Test
    fun testThatHomeFragmentIsDisplayedFirst() {
        val expectedIntent = activity.intentFor<OnboardingActivity>().newTask().clearTask()
        val actualIntent = ShadowApplication.getInstance().nextStartedActivity
        assertEquals(actualIntent.component, expectedIntent.component)
    }
}
