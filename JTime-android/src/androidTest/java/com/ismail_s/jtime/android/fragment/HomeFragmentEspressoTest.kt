package com.ismail_s.jtime.android.fragment

import android.support.test.espresso.Espresso.onData
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.runner.AndroidJUnit4
import com.codetroopers.betterpickers.calendardatepicker.YearPickerView
import com.ismail_s.jtime.android.MainActivityEspressoTestBase
import com.ismail_s.jtime.android.R
import org.hamcrest.Matchers.*
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class HomeFragmentEspressoTest: MainActivityEspressoTestBase() {

    @Test
    fun testHomeFragmentDisplaysTimes() {
        val checkTextIsDisplayedAtPosition = {cardViewId: Int, x: Int, y: Int, text: String ->
            onView(allOf(isDescendantOfA(withId(cardViewId)), atTablePosition(x, y))).check(matches(allOf(isCompletelyDisplayed(), withText(text))))
        }
        onView(allOf(isDescendantOfA(withId(R.id.home_fragment_card_fajr)), atTablePosition(0, 0), withText("Fajr"))).check(matches(isDisplayed()))
        for ((x, y, text) in listOf(Triple(1, 1, "05:30"), Triple(1, 0, "two"), Triple(2, 1, "06:00"), Triple(2, 0, "one")))
            checkTextIsDisplayedAtPosition(R.id.home_fragment_card_fajr, x, y, text)
        onView(allOf(isDescendantOfA(withId(R.id.home_fragment_card_zohar)), atTablePosition(0, 0), anyOf(withText("Zohar"), withText("Jumma")))).check(matches(isDisplayed()))
        for ((x, y, text) in listOf(Triple(1, 1, "12:25"), Triple(1, 0, "one")))
            checkTextIsDisplayedAtPosition(R.id.home_fragment_card_zohar, x, y, text)
    }

    @Test
    fun testCanChangeSearchRadiusOnHomeFragment() {
        openOverflowMenu()
        onView(allOf(withId(R.id.title), withText("Change search radius"))).perform(click())

        onView(allOf(withId(R.id.delete), withContentDescription("Delete"), withParent(withId(R.id.number_view_container)), isDisplayed())).perform(click())
        onView(allOf(withId(R.id.delete), withContentDescription("Delete"), withParent(withId(R.id.number_view_container)), isDisplayed())).perform(click())
        onView(allOf(withId(R.id.key_left), withText("1"), withParent(withId(R.id.first)), isDisplayed())).perform(click())
        onView(allOf(withId(R.id.key_right), withText("9"), withParent(withId(R.id.third)), isDisplayed())).perform(click())
        onView(allOf(withId(R.id.key_right), withText("9"), withParent(withId(R.id.third)), isDisplayed())).perform(click())
        onView(allOf(withId(R.id.done_button), withText("OK"), withParent(withId(R.id.ok_cancel_buttons_layout)), isDisplayed())).perform(click())

        onView(withText("search radius test masjid")).check(matches(isCompletelyDisplayed()))
    }

    @Test
    fun testCanChangeDateOnHomeFragment() {
        openOverflowMenu()
        onView(allOf(withId(R.id.title), withText("Change date"), isDisplayed())).perform(click())

        onView(allOf(withId(R.id.date_picker_year), withParent(withId(R.id.day_picker_selected_date_layout)), isDisplayed())).perform(click())
        onData(allOf(`is`(instanceOf(String::class.java)), `is`("1900"))).inAdapterView(`is`(instanceOf(YearPickerView::class.java))).perform(click())
        onView(allOf(withId(R.id.done_button), withText("OK"), withParent(withId(R.id.ok_cancel_buttons_layout)), isDisplayed())).perform(click())

        onView(withText("change date test masjid")).check(matches(isCompletelyDisplayed()))
    }
}
