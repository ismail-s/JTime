package com.ismail_s.jtime.android

import android.support.test.InstrumentationRegistry
import org.junit.Rule
import org.junit.rules.RuleChain
import org.junit.rules.TestRule
import org.junit.runners.model.Statement

open class MainActivityEspressoTestBase: EspressoTestBase() {

    @Rule
    @JvmField
    val chain: TestRule = RuleChain
            .outerRule(setUpMocksRule)
            .around(stopOnboardingActivityBeingLaunched)
            .around(activityRule)
}
