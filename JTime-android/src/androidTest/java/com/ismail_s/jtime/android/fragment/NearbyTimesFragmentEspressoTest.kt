package com.ismail_s.jtime.android.fragment

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.runner.AndroidJUnit4
import com.ismail_s.jtime.android.MainActivityEspressoTestBase
import com.ismail_s.jtime.android.R
import org.hamcrest.Matchers.*
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class NearbyTimesFragmentEspressoTest : MainActivityEspressoTestBase() {

    @Test
    fun testCanSeeNearbyMasjidTimes() {
        val checkTextIsDisplayedAtPosition = {x: Int, y: Int, text: String ->
            onView(atTablePosition(x, y)).check(matches(allOf(isCompletelyDisplayed(), withText(text))))
        }
        onView(withId(R.id.home_fragment_card_fajr)).perform(click())
        onView(withId(R.id.label_salaah_name)).check(matches(allOf(isCompletelyDisplayed(), withText(containsString("Fajr")))))
        sleepForSplitSecond()
        for ((x, y, text) in listOf(Triple(0, 1, "05:30"), Triple(1, 1, "06:00"), Triple(1, 0, "one"), Triple(0, 0, "two")))
            checkTextIsDisplayedAtPosition(x, y, text)

        activity.runOnUiThread { activity.onBackPressed() }
        sleepForSplitSecond()
        onView(withId(R.id.home_fragment_card_zohar)).perform(click())
        onView(withId(R.id.label_salaah_name)).check(matches(allOf(isCompletelyDisplayed(),
                anyOf(withText(containsString("Zohar")), withText(containsString("Jumma"))))))
        sleepForSplitSecond()
        for ((x, y, text) in listOf(Triple(0, 1, "12:25"), Triple(0, 0, "one")))
            checkTextIsDisplayedAtPosition(x, y, text)
    }
}
