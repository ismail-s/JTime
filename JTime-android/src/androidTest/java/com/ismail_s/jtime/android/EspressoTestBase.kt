package com.ismail_s.jtime.android

import android.location.Location
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.*
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.view.View
import android.view.WindowManager.LayoutParams.*
import android.widget.TableLayout
import android.widget.TableRow
import com.ismail_s.jtime.android.MockWebServer.createMockWebServerAndConnectToRestClient
import nl.komponents.kovenant.deferred
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Before
import org.junit.Rule
import org.junit.rules.RuleChain
import org.junit.rules.TestRule
import org.junit.runners.model.Statement

@LargeTest
open class EspressoTestBase {

    protected val setUpMocksRule: TestRule = TestRule { base, _ ->
        object : Statement() {
            override fun evaluate() {
                MainActivity.Companion.googleApiClient = MockGoogleApiClient()
                createMockWebServerAndConnectToRestClient()
                mockOutLocation()
                base.evaluate()
            }
        }
    }

    protected val stopOnboardingActivityBeingLaunched = setWhetherOnboardingActivityShouldBeLaunchedOnStartup(false)

    protected val makeSureOnboardingActivityIsLaunched = setWhetherOnboardingActivityShouldBeLaunchedOnStartup(true)

    protected val activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    protected val activity: MainActivity
        get() = activityRule.activity

    protected val all_masjids_text: String by lazy {
        getString(R.string.drawer_item_all_masjids)
    }

    @Before
    open fun setUp() {
        val wakeUpDevice = Runnable { activity.window.addFlags(
                FLAG_TURN_SCREEN_ON or FLAG_SHOW_WHEN_LOCKED or FLAG_KEEP_SCREEN_ON) }
        activity.runOnUiThread(wakeUpDevice)
        sleepForSplitSecond()
    }

    private fun mockOutLocation() {
        val mockLocation = Location("mock location")
        mockLocation.latitude = 51.507
        mockLocation.longitude = -0.1275
        val newDeferred = deferred<Location, Exception>()
        newDeferred resolve mockLocation
        MainActivity.locationDeferred = newDeferred
    }

    protected fun getString(resId: Int): String = InstrumentationRegistry.getInstrumentation().targetContext.getString(resId)

    protected fun sleepForSplitSecond() = try {
        Thread.sleep(200)
    } catch (e: InterruptedException) {
        throw RuntimeException("Thread running tests was interrupted.")
    }

    protected fun openOverflowMenu() {
        Espresso.openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().targetContext)
    }

    protected fun swipeInNavigationDrawer() {
        swipeInDrawer(GeneralLocation.CENTER_LEFT, GeneralLocation.CENTER_RIGHT)
    }

    private fun swipeInDrawer(startCoord: CoordinatesProvider, endCoord: CoordinatesProvider) {
        val swipe = ViewActions.actionWithAssertions(GeneralSwipeAction(Swipe.FAST,
                startCoord, endCoord, Press.FINGER))
        Espresso.onView(ViewMatchers.withId(R.id.fragment_container)).perform(swipe)
        sleepForSplitSecond()
    }

    protected fun atTablePosition(x: Int, y: Int): Matcher<View> {
        return object: TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("is at position $x, $y")
            }

            override fun matchesSafely(item: View): Boolean {
                val tableRow: TableRow = item.parent as? TableRow ?: return false
                val table: TableLayout = tableRow.parent as? TableLayout ?: return false
                if (table.indexOfChild(tableRow) != x || tableRow.indexOfChild(item) != y)
                    return false
                return true
            }

        }
    }

    protected fun clickOnMasjidNameToOpenMasjidFragment() {
        // Go to AllMasjidsFragment
        sleepForSplitSecond()
        swipeInNavigationDrawer()
        onView(allOf(withId(R.id.material_drawer_name), withText(all_masjids_text))).perform(click())

        // Go to MasjidsFragment
        onView(allOf(withId(R.id.content), withText("one"))).perform(click())
    }
}

private fun setWhetherOnboardingActivityShouldBeLaunchedOnStartup(launch: Boolean) = TestRule { base, _ ->
    object: Statement() {
        override fun evaluate() {
            val context = InstrumentationRegistry.getInstrumentation().targetContext
            SharedPreferencesWrapper(context).firstTimeUsingApp = launch
            base.evaluate()
        }
    }
}
