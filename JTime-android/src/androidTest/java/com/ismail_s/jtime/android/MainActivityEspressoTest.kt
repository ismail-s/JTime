package com.ismail_s.jtime.android

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.runner.AndroidJUnit4
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Matchers.allOf
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityEspressoTest: MainActivityEspressoTestBase() {

    @Test
    fun testMenuButtonOpensDrawer() {
        onView(allOf(withId(R.id.material_drawer_name), withText(all_masjids_text))).check(matches(not(isDisplayed())))
        onView(withContentDescription(getString(R.string.material_drawer_open))).perform(click())
        onView(allOf(withId(R.id.material_drawer_name), withText(all_masjids_text))).check(matches(isDisplayed()))
   }

    @Test
    fun testBackButtonClosesDrawer() {
        onView(withContentDescription(getString(R.string.material_drawer_open))).perform(click())
        activity.runOnUiThread { activity.onBackPressed() }
        sleepForSplitSecond()
        onView(allOf(withId(R.id.material_drawer_name), withText(all_masjids_text))).check(matches(not(isDisplayed())))
    }

    @Test
    fun testNavigationDrawerHasButtonToReturnToMasjidsList() {
        clickOnMasjidNameToOpenMasjidFragment()
        swipeInNavigationDrawer()
        onView(allOf(withId(R.id.material_drawer_name), withText(all_masjids_text))).perform(click())
        onView(allOf(withId(R.id.content), withText("one"))).check(matches(isCompletelyDisplayed()))
    }
}
