package com.ismail_s.jtime.android

import android.Manifest
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.swipeLeft
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.rule.GrantPermissionRule
import android.support.test.runner.AndroidJUnit4
import android.text.Html
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.junit.rules.TestRule
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class OnboardingActivityTest: EspressoTestBase() {

    @Rule
    @JvmField
    val chain: TestRule = RuleChain
            .outerRule(setUpMocksRule)
            .around(makeSureOnboardingActivityIsLaunched)
            .around(activityRule)

    @Rule
    @JvmField
    val grantPermissionRule = GrantPermissionRule.grant(Manifest.permission.ACCESS_FINE_LOCATION)

    // TODO-add tests for when location permission hasn't been granted. This will require using PowerMock as GrantPermissionRule doesn't allow revoking permissions

    @Test
    fun onboardingActivitySwipeTest() {
        runOnboardingActivityTestByMovingBetweenFragmentsBy(this::swipeToNextSlide)
    }

    @Test
    fun onboardingActivityClickTest() {
        runOnboardingActivityTestByMovingBetweenFragmentsBy(this::clickButtonToGoToNextSlide)
    }

    @Test
    fun canSkipOnboardingByClickingSkipButton() {
        onView(allOf(withId(R.id.skip), isDisplayed())).perform(click())
        checkMainActivityIsDisplayed()
    }

    private fun runOnboardingActivityTestByMovingBetweenFragmentsBy(moveToNextFragment: () -> Unit) {
        onView(allOf(withId(R.id.title), isDisplayed()))
                .check(matches(withText(getString(R.string.app_name))))
        onView(allOf(withId(R.id.description), isDisplayed()))
                .check(matches(withText(getString(R.string.onboarding_description_slide_1))))

        moveToNextFragment()

        onView(allOf(withId(R.id.title), isDisplayed()))
                .check(matches(withText(getString(R.string.onboarding_title_how_it_works))))
        onView(allOf(withId(R.id.description), isDisplayed()))

                .check(matches(withText(Html.fromHtml(getString(R.string.onboarding_description_slide_2), Html.FROM_HTML_MODE_LEGACY).toString())))

        moveToNextFragment()

        onView(allOf(withId(R.id.title), isDisplayed()))
                .check(matches(withText(getString(R.string.onboarding_title_how_it_works))))
        onView(allOf(withId(R.id.description), isDisplayed()))
                .check(matches(withText(getString(R.string.onboarding_description_slide_3))))

        moveToNextFragment()

        onView(allOf(withId(R.id.title), isDisplayed()))
                .check(matches(withText(getString(R.string.onboarding_title_how_it_works))))
        onView(allOf(withId(R.id.description), isDisplayed()))
                .check(matches(withText(getString(R.string.onboarding_description_slide_4))))

        onView(allOf(withId(R.id.done), isDisplayed())).perform(click())

        checkMainActivityIsDisplayed()
    }

    private fun checkMainActivityIsDisplayed() {
        onView(withId(R.id.fragment_container)).check(matches(isDisplayed()))
    }

    private fun clickButtonToGoToNextSlide() {
        onView(allOf(withId(R.id.next), isDisplayed())).perform(click())
    }

    private fun swipeToNextSlide() {
        onView(allOf(withId(R.id.view_pager), isDisplayed())).perform(swipeLeft())
    }
}
