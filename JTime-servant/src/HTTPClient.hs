{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedStrings #-}

module HTTPClient where

import Control.Monad.IO.Class (liftIO, MonadIO)
import Control.Monad.Reader.Class (MonadReader)
import Data.Aeson
import Data.Has (Has)
import Data.Text (Text)
import Data.Time.Clock.POSIX (getPOSIXTime)
import Data.Proxy
import GHC.Generics
import Network.HTTP.Client (Manager)
import Network.HTTP.Client.TLS (newTlsManager)
import Servant.API
import Servant.Client
import Magicbane (askObj)

newtype ReverseGeoCodeResult = ReverseGeoCodeResult
  { humanReadableAddress :: Maybe Text
  } deriving (Show, Generic)

instance FromJSON ReverseGeoCodeResult where
  parseJSON = withObject "reverseGeoCodeResult" $ \o -> do
    results <- o .:? "results"
    case results of
      Just (firstResult:_) -> ReverseGeoCodeResult <$> firstResult .:? "formatted_address"
      _ -> return $ ReverseGeoCodeResult Nothing

newtype TimeZoneResult = TimeZoneResult
  { timeZoneId :: Maybe Text } deriving (Show, Generic)

instance FromJSON TimeZoneResult

data LoginTokenResult = LoginTokenResult
  { aud :: Text
  , email :: Text
  , sub :: Int } deriving (Show, Generic)

instance FromJSON LoginTokenResult where
  parseJSON = withObject "LoginTokenResult" $ \ v ->
    LoginTokenResult <$> v .: "aud"
               <*> v .: "email"
               <*> (read <$> v .: "sub")

type MapsAPI = "geocode" :> "json" :> QueryParam "language" Text :> QueryParam "latlng" Text :> Get '[JSON] ReverseGeoCodeResult
       :<|> "timezone" :> "json" :> QueryParam "location" Text :> QueryParam "timestamp" Int :> Get '[JSON] TimeZoneResult

type LoginTokenAPI = "oauth2" :> "v3" :> "tokeninfo" :> QueryParam "id_token" Text :> Get '[JSON] LoginTokenResult

type family PrependKeyQueryParam api where
  PrependKeyQueryParam (a :<|> b) = PrependKeyQueryParam a :<|> PrependKeyQueryParam b
  PrependKeyQueryParam leaf = QueryParam "key" Text :> leaf

type family PrependCommonUrlParts api where
  PrependCommonUrlParts l = "maps" :> "api" :> l

type API = PrependCommonUrlParts (PrependKeyQueryParam MapsAPI) :<|> LoginTokenAPI

api :: Proxy API
api = Proxy

reverseGeocode' :: Maybe Text -> Maybe Text -> Maybe Text -> ClientM ReverseGeoCodeResult

timezone' :: Maybe Text -> Maybe Text -> Maybe Int -> ClientM TimeZoneResult

verifyLoginToken' :: Maybe Text -> ClientM LoginTokenResult

(reverseGeocode' :<|> timezone') :<|> verifyLoginToken' = client api

baseUrl :: String -> BaseUrl
baseUrl url = BaseUrl Https url 443 ""

getGoogleMapsClientEnv :: Manager -> ClientEnv
getGoogleMapsClientEnv = flip ClientEnv $ baseUrl "maps.googleapis.com"

-- | Perform reverse geocoding to try and get an address for a given location
reverseGeocode :: MonadIO m =>
  Maybe Text -- ^ API key
  -> Maybe Text -- ^ latitude/longitude pair separated with a comma eg 61.2,-149.9
  -> Manager -- ^ HTTP client manager
  -> m (Either ServantError ReverseGeoCodeResult)
reverseGeocode key latlng manager =
  liftIO $ runClientM (reverseGeocode' key (Just "en") latlng) (getGoogleMapsClientEnv manager)

-- | Get the timezone for a given location
timezone :: MonadIO m =>
  Maybe Text -- ^ API key
  -> Maybe Text -- ^ latitude/longitude pair separated with a comma eg 61.2,-149.9
  -> Manager -- ^ HTTP client manager
  -> m (Either ServantError TimeZoneResult)
timezone key location manager = do
  now <- liftIO $ Just . round <$> getPOSIXTime
  liftIO $ runClientM (timezone' key location now) (getGoogleMapsClientEnv manager)

verifyLoginToken :: (Has Manager a, MonadReader a m, MonadIO m) =>
    Maybe Text -> m (Either ServantError LoginTokenResult)
verifyLoginToken loginToken = do
  manager <- askObj
  let clientEnv = ClientEnv manager (baseUrl "www.googleapis.com")
  liftIO $ runClientM (verifyLoginToken' loginToken) clientEnv

getManager :: IO Manager
getManager = newTlsManager
