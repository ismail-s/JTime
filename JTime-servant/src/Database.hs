{-# LANGUAGE OverloadedStrings, DataKinds, TypeOperators, TemplateHaskell #-}
{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}

module Database where

import Control.Monad.IO.Class (liftIO, MonadIO)
import Control.Monad.Logger
import Control.Monad.Reader.Class (MonadReader)
import Control.Monad.Trans.Reader (ReaderT)
import Control.Monad.Trans.Resource (ResourceT)
import Database.Persist
import Database.Persist.Sqlite
import Database.Persist.TH
import qualified Data.ByteString.Lazy as BL
import Data.Text.Encoding (encodeUtf8)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Time.Clock (UTCTime)
import Data.Aeson (eitherDecode)
import Data.Aeson.Types
import Data.Has
import Data.String (fromString)
import GHC.Generics (Generic)
import Magicbane (askObj)
import Utils (utcTimeToString)
import Web.HttpApiData (FromHttpApiData(..))

data SalaahType = Fajr | Zohar | Asr | Magrib | Esha
    deriving Eq

instance Show SalaahType where
  show Fajr = "f"
  show Zohar = "z"
  show Asr = "a"
  show Magrib = "m"
  show Esha = "e"

instance Read SalaahType where
  readsPrec _ (c:rest) = case c of
    'f' -> res Fajr
    'z' -> res Zohar
    'a' -> res Asr
    'm' -> res Magrib
    'e' -> res Esha
    _ -> []
    where res x = [(x, rest)]
  readsPrec _ _ = []
derivePersistField "SalaahType"

instance FromJSON SalaahType where
  parseJSON = withText "SalaahType" $ \t ->
                  if T.compareLength t 1 == EQ && elem (T.head t) ("fzame" :: String)
                    then pure $ read (T.unpack t)
                    else fail "Expected one of f, z, a, m, e"

instance ToJSON SalaahType where
  toJSON = fromString . show

instance FromHttpApiData SalaahType where
  parseQueryParam "f" = Right Fajr
  parseQueryParam "z" = Right Zohar
  parseQueryParam "a" = Right Asr
  parseQueryParam "m" = Right Magrib
  parseQueryParam "e" = Right Esha
  parseQueryParam _ = Left "error"

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
AccessToken sql=accesstoken
  Id Text sqltype=varchar(1024)
  ttl Int
  created UTCTime default=CURRENT_TIME
  userid UserId
  deriving Show
User sql=user_table
  realm Text Maybe sqltype=varchar(1024)
  username Text sqltype=varchar(1024)
  UniqueUsername username
  password Text sqltype=varchar(1024)
  credentials Text Maybe sqltype=varchar(1024)
  challenges Text Maybe sqltype=varchar(1024)
  email Text sqltype=varchar(1024)
  UniqueEmail email
  emailverified Bool Maybe
  verificationtoken Text Maybe sqltype=varchar(1024)
  status Text Maybe sqltype=varchar(1024)
  created UTCTime default=CURRENT_TIME
  lastupdated UTCTime default=CURRENT_TIME
  Id Int
  deriving Eq Read Show
Masjid json
  name Text sqltype=varchar(50)
  latitude Double
  longitude Double
  UniqueLocation longitude latitude
  humanReadableAddress Text Maybe sql=humanreadableaddress sqltype=varchar(100)
  timeZoneId Text Maybe sql=timezoneid sqltype=varchar(100)
  createdat UTCTime default=CURRENT_TIME
  lastmodifiedat UTCTime default=CURRENT_TIME
  Id Int
  deriving Show Generic
SalaahTime sql=salaahtime
  type SalaahType sqltype=varchar(1)
  masjidid MasjidId
  datetime UTCTime
  createdat UTCTime default=CURRENT_TIME
  lastmodifiedat UTCTime default=CURRENT_TIME
  Id Int
  deriving Show
|]

-- | Create a user
createUser :: Text -- ^ username
  -> Text -- ^ password
  -> Text -- ^ email
  -> UTCTime -- ^ creation datetime
  -> UTCTime -- ^ last updated datetime
  -> User -- ^ the 'User'
createUser username password email = User Nothing username password Nothing Nothing email Nothing Nothing Nothing

instance FromJSON (Entity User) where
  parseJSON = withObject "User" $ \ v ->
    let key = UserKey <$> v .: "id"
        user = createUser <$> v .: "username"
               <*> v .: "password"
               <*> v .: "email"
               <*> v .: "created"
               <*> v .: "lastupdated"
    in Entity <$> key <*> user

instance ToJSON (Entity User) where
  toJSON (Entity (UserKey key) (User _ username password _ _ email _ _ _ created lastupdated)) =
    object [ "id" .= key
           , "username" .= username
           , "password" .= password
           , "email" .= email
           , "created" .= created
           , "lastupdated" .= lastupdated ]

data Location = Location
  {lat :: Double,
   lng :: Double} deriving (Show, Generic)

instance ToJSON Location
instance FromJSON Location

instance FromHttpApiData Location where
  parseUrlPiece locString = case eitherDecode (BL.fromStrict $ encodeUtf8 locString) of
    Left errorMsg -> Left $ T.pack $ "failed to decode location param: " ++ errorMsg
    Right res -> Right res

instance ToJSON (Entity SalaahTime) where
  toJSON (Entity (SalaahTimeKey key) (SalaahTime salaahtype masjidid datetime createdat lastupdatedat)) =
    object [ "id" .= key
           , "type" .= salaahtype
           , "masjidid" .= masjidid
           , "datetime" .= utcTimeToString datetime
           , "createdat" .= createdat
           , "lastupdatedat" .= lastupdatedat ]

instance ToJSON SalaahTime where
  toJSON (SalaahTime salaahtype masjidid datetime createdat lastupdatedat) =
    object [ "type" .= salaahtype
           , "masjidid" .= masjidid
           , "datetime" .= utcTimeToString datetime
           , "createdat" .= createdat
           , "lastupdatedat" .= lastupdatedat ]

-- | Perform a persistent SQL action in the 'Types.MyApp' monad
doSql :: (MonadIO m, MonadReader a m, Has ConnectionPool a) =>
               ReaderT SqlBackend (NoLoggingT (ResourceT IO)) b -> m b
doSql action = do
  pool <- askObj
  liftIO $ runSqlPersistMPool action (pool :: ConnectionPool)

-- | Create and return an sqlite connection pool for the sqlite db
-- at the given filepath
getPool :: Text -- ^ path to sqlite db file
  -> IO ConnectionPool
getPool filepath = runStderrLoggingT $ createSqlitePool filepath 1
