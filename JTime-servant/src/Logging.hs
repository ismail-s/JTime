{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE FlexibleInstances, FlexibleContexts, UndecidableInstances, OverloadedStrings #-}

module Logging where

import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Logger (MonadLogger(..), MonadLoggerIO(..), Loc, LogSource, LogLevel, LogStr, defaultLogStr, logInfoN, toLogStr)
import Control.Monad.Reader (MonadReader, asks)
import Data.Has (Has, getter)
import qualified Data.Text as T
import Data.Text (Text, pack)
import System.Log.FastLogger (LogType, TimedFastLogger, newTimedFastLogger, newTimeCache, simpleTimeFormat')

newtype ModLogger = ModLogger (Loc -> LogSource -> LogLevel -> LogStr -> IO ())

instance (Has ModLogger a, Monad b, MonadIO b, MonadReader a b) => MonadLogger b where
  monadLoggerLog loc src lvl msg = asks getter >>= \(ModLogger f) -> liftIO (f loc src lvl $ toLogStr msg)

instance (Has ModLogger a, MonadIO b, MonadReader a b) => MonadLoggerIO b where
  askLoggerIO = (\(ModLogger f) -> f) <$> asks getter

-- | Creates a logger module. Also returns the logger itself for using outside of your Magicbane app (e.g. in some WAI middleware).
newLogger :: LogType -> IO (TimedFastLogger, ModLogger)
newLogger logtype = do
  tc <- newTimeCache simpleTimeFormat'
  (fl, _) <- newTimedFastLogger tc logtype
  -- forget cleanup because the logger will exist for the lifetime of the (OS) process
  return (fl, ModLogger $ \loc src lvl msg -> fl (\t -> toLogStr (t `mappend` " ") `mappend` defaultLogStr loc src lvl msg))

-- | Log a string and a 'Show'-able object
logInfoNSH :: (MonadLogger m, Show a) => Text -> a -> m ()
logInfoNSH msg s = logInfoN $ msg `T.append` pack (show s)
