{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
module Settings where

import Control.Exception (try)
import Data.Aeson (FromJSON, decode)
import qualified Data.ByteString.Lazy as B
import Data.Text (Text)
import GHC.Generics (Generic)
import Utils (eitherToMaybe)

data ApplicationSettings = ApplicationSettings {
  clientId :: Text,
  googleMapsKey :: Text,
  minDistanceBetweenMasjidsWithSameName :: Double,
  minDistanceBetweenMasjidsWithDiffName :: Double
  } deriving Generic

instance FromJSON ApplicationSettings

-- | Default app settings.
defaultAppSettings :: ApplicationSettings
defaultAppSettings = ApplicationSettings
  {clientId = "TODO-put client id here",
   googleMapsKey = "TODO-put key here...",
   minDistanceBetweenMasjidsWithSameName = 75,
   minDistanceBetweenMasjidsWithDiffName = 50}

-- | Get settings for the server from a file in the current working
-- directory called @settings.json@. If there are issues getting or
-- parsing the settings, then 'defaultAppSettings' is used to
-- configure the app.
getApplicationSettings :: IO ApplicationSettings
getApplicationSettings = do
  eitherSettingsFile <- try $ B.readFile "settings.json" :: IO (Either IOError B.ByteString)
  let maybeSettings = eitherToMaybe eitherSettingsFile >>= decode
  case maybeSettings of
    Nothing -> do
      putStrLn "Using default settings"
      return defaultAppSettings
    Just settings -> do
      putStrLn "Got settings from settings.json file"
      return settings
