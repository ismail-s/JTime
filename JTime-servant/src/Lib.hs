{-# LANGUAGE OverloadedStrings #-}

module Lib
    ( main
    ) where

import Conduit ((.|), filterC, mapC, sinkList, runConduit)
import Control.Concurrent (forkIO, threadDelay)
import Control.Monad (void, forever, unless, guard)
import Data.Text (Text)
import qualified Data.Text as T
import Database.Persist.Sqlite
import Magicbane
import Authentication
import Routes.Masjid
import Routes.SalaahTime
import Routes.User
import Servant
import Database
import Logging (newLogger)
import HTTPClient (getManager)
import Middleware (lowercaseUrlMiddleware)
import Settings (getApplicationSettings)
import Types
import Data.Has
import Data.Time.Clock (getCurrentTime)

import Network.Wai.Handler.Warp (run)
import System.Environment (lookupEnv)
import System.IO (hPutStrLn, stderr)
import System.FilePath (isValid)
import System.Log.FastLogger (LogType(..), defaultBufSize)
import Text.Read (readMaybe)

masjidHandlers :: ServerT MasjidRoutes MyApp
masjidHandlers = getAllMasjids :<|> const createNewMasjid :<|> timesHandlers where
  timesHandlers masjidId = getTimes masjidId :<|> getTimesForToday masjidId :<|> getTimesForAMonth masjidId

salaahTimeHandlers :: ServerT SalaahTimeRoutes MyApp
salaahTimeHandlers = const (createOrUpdateSalaahTimes :<|> createOrUpdateMultipleSalaahTimes) :<|> (\lat' lng' loc salaahType searchRadius fMasjidIds -> getTimesForMultipleMasjids lat' lng' loc salaahType searchRadius fMasjidIds :<|> getTimesForMultipleMasjidsForToday lat' lng' loc salaahType searchRadius fMasjidIds)

userHandlers :: ServerT UserRoutes MyApp
userHandlers = login :<|> (\a -> getUser a :<|> logout a)

server :: ServerT RestAPI MyApp
server = masjidHandlers :<|> salaahTimeHandlers :<|> userHandlers

app :: MyAppContext -> Application
app ctx = serveWithContext finalAPI (authServerContext (getter ctx)) (enter (magicbaneToExcept ctx) server :<|> serveDirectoryFileServer "staticRoot")

getPort :: IO Int
getPort = do
  maybePort <- (>>= readMaybe) <$> lookupEnv "PORT"
  case maybePort of
    Nothing -> do
      hPutStrLn stderr "Weren't able to read PORT env var, using default value"
      return 8082
    Just port -> do
      hPutStrLn stderr "Using PORT env var to set port we listen on"
      return port

getDbFilepath :: IO Text
getDbFilepath = do
  maybeFilepath' <- lookupEnv "DB_FILE_LOCATION"
  let maybeFilepath = maybeFilepath' >>= (\f -> guard (isValid f) >> return (T.pack f))
  case maybeFilepath of
    Nothing -> do
      hPutStrLn stderr "Weren't able to read DB_FILE_LOCATION env var, using default value test.db"
      return "test.db"
    Just filepath -> do
      hPutStrLn stderr "Using DB_FILE_LOCATION env var as the location of the sqlite db file"
      return filepath

pruneAccessTokenDb :: ConnectionPool -> IO ()
pruneAccessTokenDb pool = void $ forkIO $ forever $ do
  currTime <- getCurrentTime
  _ <- flip runSqlPersistMPool pool $ do
    tokens <- runConduit $ selectSource [] [] .| filterC (isTokenExpired currTime) .| mapC entityKey .| sinkList
    unless (null tokens) $ deleteWhere [AccessTokenId <-. tokens]
  threadDelay $ 1000000 * 60 * 60 * 24 -- wait for 1 day

-- | Run the Rest API
main :: IO ()
main = do
  port <- getPort
  dbFilePath <- getDbFilepath
  pool <- getPool dbFilePath
  manager <- getManager
  (_, modLog) <- newLogger $ LogStdout defaultBufSize
  applicationSettings <- getApplicationSettings
  hPutStrLn stderr $ "Using sqlite file " ++ T.unpack dbFilePath
  runSqlPool (runMigration migrateAll) pool
  pruneAccessTokenDb pool
  let ctx = (pool, manager, applicationSettings, modLog)
  hPutStrLn stderr $ "Running on port " ++ show port
  run port $ lowercaseUrlMiddleware $ app ctx
