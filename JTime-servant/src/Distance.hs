module Distance (distanceBetween) where

import Geodetics.Geodetic (groundDistance, Geodetic(..), WGS84(..))
import qualified Naqsha
import qualified Naqsha.Geometry.Spherical as Naqsha
import Numeric.Units.Dimensional ((*~), (/~), _0)
import Numeric.Units.Dimensional.SIUnits (degree, metre)

-- | Compute the distance between 2 points on the Earth's surface in metres.
-- Internally, we try to use 'groundDistance' (which uses Vincenty's
-- formula) and fallback to 'Naqsha.distance' (which uses the Haversine
-- formula).
distanceBetween :: (Double, Double) -> (Double, Double) -> Double
distanceBetween a'@(aLat, aLng) b'@(bLat, bLng) = if a' == b' then 0 else let
  a = Geodetic (aLat *~ degree) (aLng *~ degree) _0 WGS84
  b = Geodetic (bLat *~ degree) (bLng *~ degree) _0 WGS84
  in case groundDistance a b of
    Nothing -> distanceBetweenHaversine (aLat, aLng) (bLat, bLng)
    Just (distanceInMetres, _, _) -> distanceInMetres /~ metre

distanceBetweenHaversine :: (Double, Double) -> (Double, Double) -> Double
distanceBetweenHaversine (aLat, aLng) (bLat, bLng) = let
  doubleToDegree = Naqsha.degree . toRational
  north = Naqsha.north . doubleToDegree
  east = Naqsha.east . doubleToDegree
  a = Naqsha.Geo (north aLat) (east aLng)
  b = Naqsha.Geo (north bLat) (east bLng)
  in Naqsha.distance a b
