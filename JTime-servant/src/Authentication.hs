{-# LANGUAGE OverloadedStrings, DataKinds, TypeOperators #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Authentication (authServerContext, isTokenExpired) where

import Control.Monad.IO.Class (liftIO)
import Data.ByteString (ByteString)
import qualified Data.ByteString.Char8 as C
import qualified Data.Text as T
import Data.Time.Clock (getCurrentTime, addUTCTime, diffUTCTime, UTCTime(..))
import Database
import Database.Persist.Sqlite (ConnectionPool, getEntity, runSqlPersistMPool)
import Database.Persist.Types
import Servant
import Servant.API.Experimental.Auth (AuthProtect)
import Servant.Server.Experimental.Auth (AuthHandler, AuthServerData, mkAuthHandler)
import Network.Wai (Request, requestHeaders)

lookupUser :: ByteString -> ConnectionPool -> Handler (Entity AccessToken)
lookupUser key' pool = do
  let key = T.pack (C.unpack key')
      getFromDb = liftIO . (`runSqlPersistMPool` pool) . getEntity
  --search for the key in accesstoken table
  maybeDbRow <- getFromDb $ AccessTokenKey key
  case maybeDbRow of
    Nothing -> throwError (err403 { errBody = "Invalid access token" })
    Just token -> do
      --if we find an in-date row, return the access token
      currTime <- liftIO getCurrentTime
      if not (isTokenExpired currTime token) then return token
      else throwError (err403 { errBody = "Token expired" })

addSecondsToUTCTime :: UTCTime -> Int -> UTCTime
addSecondsToUTCTime time = flip addUTCTime time . fromInteger . toInteger

-- | Check if an access token is valid or not
isTokenExpired :: UTCTime -- ^ the current time
  -> Entity AccessToken -- ^ the access token to check
  -> Bool -- ^ whether the token is valid or not
isTokenExpired currTime (Entity _ (AccessToken ttl created _)) = expiryCheck where
  expiryDate = addSecondsToUTCTime created ttl
  expiryCheck = diffUTCTime expiryDate currTime <= 0

-- | The auth handler wraps a function from Request -> Handler Account
authHandler :: ConnectionPool -> AuthHandler Request (Entity AccessToken)
authHandler pool =
  let handler req = case lookup "Authorization" (requestHeaders req) of
        Nothing -> throwError (err403 { errBody = "Missing auth header" })
        Just accessToken -> lookupUser accessToken pool
  in mkAuthHandler handler

-- | We need to specify the data returned after authentication
type instance AuthServerData (AuthProtect "token-auth") = Entity AccessToken

-- | The context that will be made available to request handlers.
authServerContext :: ConnectionPool -> Context (AuthHandler Request (Entity AccessToken) ': '[])
authServerContext pool = authHandler pool :. EmptyContext
