{-# LANGUAGE DataKinds, TypeOperators #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Magicbane where

import Servant
import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Reader
import Control.Monad.Base (MonadBase)
import Control.Monad.Catch (MonadThrow, MonadCatch)
import Control.Monad.Error.Class (MonadError)
import Control.Monad.Except (ExceptT(..), runExceptT)
import Control.Monad.Trans.Control (MonadBaseControl(..))
import Data.Has

newtype MagicbaneApp b a = MagicbaneApp {
  unMagicbaneApp :: ReaderT b Handler a
} deriving (Functor, Applicative, Monad, MonadIO, MonadBase IO,
            MonadThrow, MonadCatch, MonadError ServantErr, MonadReader b)

instance MonadBaseControl IO (MagicbaneApp b) where
  type StM (MagicbaneApp b) a = StM (ReaderT b Handler) a
  liftBaseWith f = MagicbaneApp $ liftBaseWith $ \x -> f $ x . unMagicbaneApp
  restoreM       = MagicbaneApp . restoreM

runMagicbaneExcept :: b -> MagicbaneApp b a -> Handler a
runMagicbaneExcept ctx a = Handler $ ExceptT $ liftIO $ runExceptT $ runHandler' $ runReaderT (unMagicbaneApp a) ctx

magicbaneToExcept :: b -> MagicbaneApp b :~> Handler
magicbaneToExcept ctx = NT $ runMagicbaneExcept ctx

-- | Gets a value of any type from the context.
askObj :: (Has b a, MonadReader a u) => u b
askObj = asks getter

-- | Gets a thing from a value of any type from the context. (Useful for configuration fields.)
askOpt :: (Has b a, MonadReader a u) => (b -> t) -> u t
askOpt f = asks $ f . getter
