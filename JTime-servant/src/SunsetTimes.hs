module SunsetTimes where

import Control.Exception (try)
import Data.Astro.CelestialObject.RiseSet (RiseSet(..))
import Data.Astro.Sun (sunRiseAndSet)
import Data.Astro.Time.JulianDate (lcdFromYMD, lctToYMDHMS)
import Data.Astro.Types (DecimalDegrees(..), GeographicCoordinates(..))
import Data.Time.Calendar (Day, toGregorian, fromGregorian)
import Data.Time.Clock (secondsToDiffTime, UTCTime(..))
import Data.Time.LocalTime (timeOfDayToTime, LocalTime(..))
import Data.Time.LocalTime.TimeZone.Olson (getTimeZoneSeriesFromOlsonFile)
import Data.Time.LocalTime.TimeZone.Series (utcToLocalTime', TimeZoneSeries)
import Data.Text (Text)
import qualified Data.Text as T

-- |Compute the sunset time for the given day at the given location
-- with the time converted into local time for the given timezone.
getSunsetTime
  :: Text -- ^ Timezone
  -> (Double, Double) -- ^ Tuple of (latitude, longitude)
  -> Day -- ^ Day to compute sunset time for
  -> IO (Maybe UTCTime) -- ^ The sunset time
getSunsetTime timezoneId (lat, lng) date = do
  eitherTzSeries <- try $ getTimeZoneSeriesFromOlsonFile $ "/usr/share/zoneinfo/" ++ T.unpack timezoneId :: IO (Either IOError TimeZoneSeries)
  let (year, month, day) = toGregorian date
      riseAndSet = sunRiseAndSet (GeoC (DD lat) (DD lng)) 0.833333 (lcdFromYMD 0 year month day)
  case riseAndSet of
    RiseSet _ set -> case set of
      Just (sunset, _) -> case eitherTzSeries of
        Right tzSeries -> do
          let (y, m, d, h, mm, s) = lctToYMDHMS sunset
              timeWithoutTimezone = UTCTime (fromGregorian y m d) (secondsToDiffTime $ toInteger $ (h * 3600) + (mm * 60) + round s)
              LocalTime localDay' timeOfDay = utcToLocalTime' tzSeries timeWithoutTimezone
          return $ Just $ UTCTime localDay' (timeOfDayToTime timeOfDay)
        Left _ -> return Nothing
      _ -> return Nothing
    _ -> return Nothing
