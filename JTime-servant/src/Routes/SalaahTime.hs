{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module Routes.SalaahTime
  (
    createOrUpdateSalaahTimes,
    createOrUpdateMultipleSalaahTimes,
    getTimesForMultipleMasjids,
    getTimesForMultipleMasjidsForToday
  ) where

import Conduit ((.|), filterC, sinkList, runConduit)
import Control.Applicative ((<|>))
import Control.Monad (forM_)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Logger (logInfoN)
import Database.Persist.Sqlite
import Database
import Distance (distanceBetween)
import Logging (logInfoNSH)
import SunsetTimes
import Types
import Data.Either (partitionEithers)
import Data.List (find, sortBy)
import Data.List.Split (chunksOf)
import Data.Maybe (catMaybes, fromMaybe, isNothing, mapMaybe)
import Data.Time.Calendar (addDays, Day)
import Data.Time.Clock (getCurrentTime, UTCTime(..))
import qualified Data.Text as T
import Utils (getTodayAndTomorrow, throwErr400)

-- | Create or update a salaah time. Parameters can be passed either
-- as query params or in the request body
createOrUpdateSalaahTimes :: Maybe (Key Masjid) -- ^ Masjid id
  -> Maybe SalaahType -- ^ Salaah type
  -> Maybe UTCTime -- ^ New salaah time to create or update
  -> Maybe CreateOrUpdateReqBody -- ^ Masjid id\/salaah type\/new salaah time as a request body
  -> MyApp CreateOrUpdateResBody
createOrUpdateSalaahTimes maybeMasjidId maybeSalaahType maybeDatetime maybeReqBody = let
  checkQueryParams = do
    masjidid <- maybeMasjidId
    salaahType <- maybeSalaahType
    datetime <- maybeDatetime
    return (masjidid, salaahType, datetime)
  checkReqBody = do
    (CreateOrUpdateReqBody masjidId salaahType datetime) <- maybeReqBody
    return (masjidId, salaahType, datetime)
  in case checkQueryParams <|> checkReqBody of
    Nothing -> throwErr400 "incorrect parameters"
    Just (masjidid, salaahType, datetime@(UTCTime day _)) -> do
      --check that masjidid is for a masjid that exists
      maybeMasjid <- doSql $ get masjidid
      if isNothing maybeMasjid then do
        logInfoN $ T.concat ["Received request to create salaah time for masjid id ", T.pack (show masjidid), " which doesn't exist"]
        throwErr400 "that masjid doesn't exist"
        else do
        let startOfDay = UTCTime day 0
            startOfNextDay = UTCTime (addDays 1 day) 0
        currentSavedSalaahTime <- doSql $ selectFirst [SalaahTimeMasjidid ==. masjidid, SalaahTimeType ==. salaahType, SalaahTimeDatetime >=. startOfDay, SalaahTimeDatetime <. startOfNextDay] []
        currTime <- liftIO getCurrentTime
        CreateOrUpdateResBody <$>
          case currentSavedSalaahTime of
            Nothing -> do
              let salaahTimeToCreate = SalaahTime salaahType masjidid datetime currTime currTime
              logInfoNSH "Creating new salaah time: " salaahTimeToCreate
              doSql $ insertRecord salaahTimeToCreate
            Just currentSalaahTime -> do
              logInfoN $ T.concat ["Updating salaah time ", T.pack (show currentSalaahTime), " with time ", T.pack (show datetime)]
              doSql $ updateGet (entityKey currentSalaahTime) [SalaahTimeDatetime =. datetime, SalaahTimeLastmodifiedat =. currTime]

-- | Create or update multiple salaah times for a single masjid.
-- Like calling 'createOrUpdateSalaahTimes' multiple times, but more efficient.
createOrUpdateMultipleSalaahTimes :: CreateOrUpdateMultipleReqBody -> MyApp CreateOrUpdateMultipleResBody
createOrUpdateMultipleSalaahTimes (CreateOrUpdateMultipleReqBody masjidid newTimes) = do
  --first, check that the masjidid is for an actual masjid
  maybeMasjid <- doSql $ get masjidid
  if isNothing maybeMasjid then throwErr400 "that masjid doesn't exist" else do
    --then, we query the db for all possible existing salaah times
    let f (SalaahTimeWithJustTypeAndDatetime salaahType (UTCTime day _)) = let
          (startOfDay, startOfNextDay) = getTodayAndTomorrow day
          in [SalaahTimeType ==. salaahType, SalaahTimeDatetime >=. startOfDay, SalaahTimeDatetime <. startOfNextDay]
        --take newTimes, create a db query for each time, chunk these queries into groups of 15,
        --then combine each chunk of queries into a single query with OR,
        --turn our list of queries into a list of monadic actions to query the db, then fmap concat,
        --which combines all the results into one list
        --the chunking stops a big query being constructed which results in a parser error
        --the number 15 was chosen as it isn't too big to cause a parser error
        chunkedQuery = (fmap concat . mapM (flip selectList [] . ((SalaahTimeMasjidid ==. masjidid):) . foldr1 (||.)) . chunksOf 15 . map f) newTimes
    existingTimes <- doSql chunkedQuery
    --we have 2 lists: ls and existingTimes. We want to construct a list of times to create and a list of times to update with the corresponding key of the existing entry.
    --map over ls, divide it using Either into lists of (key, newRecord) and recordToInsert
    currTime <- liftIO getCurrentTime
    let g (SalaahTimeWithJustTypeAndDatetime salaahType datetime@(UTCTime day _)) = let
          searchFunc u = let x = entityVal u in
            salaahTimeType x == salaahType && utctDay (salaahTimeDatetime x) == day
          salaahTime = SalaahTime salaahType masjidid datetime currTime currTime
          searchForMatchingSalaahTime = entityKey <$> find searchFunc existingTimes in
          case searchForMatchingSalaahTime of
            Nothing -> Right salaahTime
            Just salaahTimeId -> Left (salaahTimeId, salaahTime)
        (timesToUpdate, timesToCreate) = partitionEithers $ map g newTimes
    logInfoNSH "Creating/updating multiple salaah times for masjid id: " masjidid
    logInfoNSH "Performing updates to existing salaah times: " timesToUpdate
    --for ones that exist already, we update them using replace
    forM_ timesToUpdate $ \(key, record) -> doSql $ replace key record
    logInfoNSH "Creating new salaah times: " timesToCreate
    --for ones that don't, we batch create them using insertMany_
    doSql $ insertMany_ timesToCreate
    return $ CreateOrUpdateMultipleResBody (timesToCreate ++ (snd <$> timesToUpdate))

-- | Get salaah times for multiple masjids for a given day.
-- The masjids to get salaah times for are either specified
-- explicitly, or a latitude, longitude and search radius (in km)
-- are provided to search for nearby masjids, or both are provided.
-- A salaah type can also be provided for further filtering.
getTimesForMultipleMasjids
  :: Maybe Double -- ^ Latitude
  -> Maybe Double -- ^ Longitude
  -> Maybe Location -- ^ Latitude and longitude as a single object.
  -> Maybe SalaahType -- ^ Salaah type
  -> Maybe Int -- ^ Search radius in km. Must be strictly between 0 and 200.
  -> [Int] -- ^ Masjid ids of masjids to return results for.
  -> Maybe UTCTime -- ^ Day to search for salaah times on.
  -> MyApp TimesForMultipleMasjidsResBody -- ^ The query results
getTimesForMultipleMasjids maybeLat maybeLng maybeLoc maybeSalaahType maybeSearchRadius faveMasjidIds maybeTime = do
  --first, we validate the input params. We need either loc, faveMasjidIds or both
  --searchRadius is optional, but if provided, must satisfy 0 < searchRadius < 200
  let searchRadius' = fromMaybe 10 maybeSearchRadius
  searchRadius <- if searchRadius' > 0 && searchRadius' < 200
    then return $ searchRadius' * 1000
    else throwErr400 "Invalid searchRadius param"
  case maybeTime of
    Nothing -> throwErr400 "date param is required"
    Just (UTCTime day _) -> case (maybeLat, maybeLng, maybeLoc, faveMasjidIds) of
      (Nothing, Nothing, Nothing, []) -> throwErr400 "one of loc or faveMasjidIds params is required"
      (Nothing, _, Nothing, _) -> throwErr400 "both lat and lng are required"
      (_, Nothing, Nothing, _) -> throwErr400 "both lat and lng are required"
      _ -> do
        let both :: (a -> b) -> (a, a) -> (b, b)
            both f (a, b) = (f a, f b)
            oneOfFaveMasjidIds masjid = unMasjidKey (entityKey masjid) `elem` faveMasjidIds
            currLocation = case (maybeLat, maybeLng, maybeLoc) of
                (Nothing, Nothing, Nothing) -> Nothing
                (Nothing, _, Nothing) -> Nothing
                (_, Nothing, Nothing) -> Nothing
                (Nothing, Just _, Just (Location lat' lng')) -> Just (lat', lng')
                (Just _, Nothing, Just (Location lat' lng')) -> Just (lat', lng')
                (Nothing, Nothing, Just (Location lat' lng')) -> Just (lat', lng')
                (Just lat', Just lng', _) -> Just (lat', lng')
            distanceFromCurrLocation loc (Entity _ m) = distanceBetween (masjidLatitude m, masjidLongitude m) loc
            filterFunc :: Entity Masjid -> Bool
            filterFunc masjid = oneOfFaveMasjidIds masjid || closeToCurrentLocation where
              m = entityVal masjid
              checkDistance (lat', lng') = distanceBetween (masjidLatitude m, masjidLongitude m) (lat', lng') <= fromIntegral searchRadius
              closeToCurrentLocation = maybe False checkDistance currLocation
            sortFunc :: Entity Masjid -> Entity Masjid -> Ordering
            sortFunc a b = let
              (aInFaveMasjidIds, bInFaveMasjidIds) = (oneOfFaveMasjidIds a, oneOfFaveMasjidIds b)
              (aDistance, bDistance) = case currLocation of
                Nothing -> both (fromIntegral . unMasjidKey . entityKey) (a, b)
                Just loc -> both (distanceFromCurrLocation loc) (a, b)
              in case compare bInFaveMasjidIds aInFaveMasjidIds of
                EQ -> compare aDistance bDistance
                res -> res
        --then we query the db to get the nearest masjids if necessary
        --then, we construct a list of fave masjids ++ nearest masjids
        --then, we query the db for details about all masjids (combine with previous db query)
        masjidList' <- doSql $ runConduit $ selectSource [] [] .| filterC filterFunc .| sinkList
        let masjidList = (take (length faveMasjidIds + 30) . sortBy sortFunc) masjidList'
        liftIO $ print masjidList
        --then, we get salaah times for those masjids for the given day
        let (startOfDay, startOfNextDay) = getTodayAndTomorrow day
            salaahTypeSubQuery = case maybeSalaahType of
              Nothing -> []
              Just t -> [SalaahTimeType ==. t]
            salaahTimeQuery = [SalaahTimeDatetime >=. startOfDay, SalaahTimeDatetime <. startOfNextDay, SalaahTimeMasjidid <-. (entityKey <$> masjidList)] ++ salaahTypeSubQuery
        timesFromDb <- doSql $ selectList salaahTimeQuery []
        --we then get magrib times for the given day if necessary
        magribTimes <- case maybeSalaahType of
          Just Magrib -> getMagribTimes masjidList day
          Nothing -> getMagribTimes masjidList day
          _ -> return []
        --combine these salaah times with magrib times
        let allTimes = (entityVal <$> timesFromDb) ++ magribTimes
            addMasjidInfoToSalaahTime t = do
              masjid <- find (\m -> salaahTimeMasjidid t == entityKey m) masjidList
              let m = entityVal masjid
                  loc = Location (masjidLatitude m) (masjidLongitude m)
              return $ ExtendedSalaahTimeNearbyTimesResult (salaahTimeMasjidid t) loc (masjidName m) (salaahTimeDatetime t) (salaahTimeType t)
            timesWithMasjidInfo = mapMaybe addMasjidInfoToSalaahTime allTimes
        --append masjid data for each time, then return
        return $ TimesForMultipleMasjidsResBody timesWithMasjidInfo

getSunsetTimeForOneMasjid :: Day -> UTCTime -> Entity Masjid -> MyApp (Maybe SalaahTime)
getSunsetTimeForOneMasjid day currTime masjid = do
  let m = entityVal masjid
      (Masjid _ lat' lng' _ maybeTimezoneId _ _) = m
  case maybeTimezoneId of
      Nothing -> return Nothing
      Just timezoneId -> do
        sunsetTime <- liftIO $ getSunsetTime timezoneId (lat', lng') day
        return $ (\t -> SalaahTime Magrib (entityKey masjid) t currTime currTime) <$> sunsetTime

getMagribTimes :: [Entity Masjid] -> Day -> MyApp [SalaahTime]
getMagribTimes masjidList day = do
  currTime <- liftIO getCurrentTime
  catMaybes <$> mapM (getSunsetTimeForOneMasjid day currTime) masjidList

-- | Like 'getTimesForMultipleMasjids', but returns times only for today,
-- where today is the day the server thinks today is.
--
-- /This function is deprecated/
getTimesForMultipleMasjidsForToday :: Maybe Double -> Maybe Double -> Maybe Location -> Maybe SalaahType -> Maybe Int -> [Int] -> MyApp TimesForMultipleMasjidsResBody
getTimesForMultipleMasjidsForToday lat' lng' loc salaahType searchRadius faveMasjidIds = do
  currTime <- liftIO getCurrentTime
  getTimesForMultipleMasjids lat' lng' loc salaahType searchRadius faveMasjidIds (Just currTime)
