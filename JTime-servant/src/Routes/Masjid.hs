{-# LANGUAGE OverloadedStrings #-}

module Routes.Masjid
  (
    getAllMasjids,
    createNewMasjid,
    getTimes,
    getTimesForToday,
    getTimesForAMonth
  ) where

import Magicbane
import Database
import Distance
import HTTPClient (timezone, reverseGeocode, ReverseGeoCodeResult(..), TimeZoneResult(..))
import Logging (logInfoNSH)
import SunsetTimes
import Types
import Conduit ((.|), anyC, mapC, runConduit)
import Servant
import Control.Concurrent.Async (concurrently)
import Control.Monad (when)
import Control.Monad.IO.Class (liftIO)
import Data.Maybe (catMaybes)
import qualified Data.Text as T
import Data.Time.Calendar (addGregorianMonthsClip, Day, toGregorian, fromGregorian, isLeapYear)
import Data.Time.Calendar.MonthDay (monthLength)
import Data.Time.Clock (getCurrentTime, UTCTime(..))
import Database.Persist.Sqlite
import Settings (ApplicationSettings(..))
import Utils (getTodayAndTomorrow)

-- | Get a list of all masjids
getAllMasjids :: MyApp [GetMasjidsResBody]
getAllMasjids = map f <$> doSql (selectList [] []) where
  f (Entity (MasjidKey key)
     (Masjid name lat' lng' humanReadableAddress' timeZoneId' createdAt lastModifiedAt)) =
    GetMasjidsResBody key name (Location lat' lng') humanReadableAddress' timeZoneId' createdAt lastModifiedAt

-- | Create a new masjid
createNewMasjid :: CreateMasjidReqBody -> MyApp (Maybe Masjid)
createNewMasjid masjid@(CreateMasjidReqBody name (Location lat' lng')) = do
  -- make sure no duplicate masjid or too close to existing masjid
  distanceBetweenMasjidsWithSameName <- askOpt minDistanceBetweenMasjidsWithSameName
  distanceBetweenMasjidsWithDiffName <- askOpt minDistanceBetweenMasjidsWithDiffName
  gMapsKey <- Just <$> askOpt googleMapsKey
  let filterFunc :: Masjid -> Bool
      filterFunc masjid' = if T.toCaseFold (masjidName masjid') == T.toCaseFold name
        then distance < distanceBetweenMasjidsWithSameName else distance < distanceBetweenMasjidsWithDiffName where
            distance = distanceBetween (lat', lng') (masjidLatitude masjid', masjidLongitude masjid')
  conflictingMasjids <- doSql $ runConduit $ selectSource [] [] .| mapC entityVal .| anyC filterFunc
  when conflictingMasjids $ do
    logInfoNSH "Found conflicting masjids when trying to create masjid: " masjid
    throwError err403 {errBody = "Too close to existing masjid"}
  -- get address and timezone
  let loc = Just (T.pack (show lat' ++ "," ++ show lng'))
  manager <- askObj
  (reverseGeocodeResults', timezoneResults') <- liftIO $ concurrently (reverseGeocode gMapsKey loc manager) (timezone gMapsKey loc manager)
  let humanReadableAddress' = case reverseGeocodeResults' of
        Left _ -> Nothing
        Right (ReverseGeoCodeResult address) -> address
      timezoneId' = case timezoneResults' of
        Left _ -> Nothing
        Right (TimeZoneResult timezoneId) -> timezoneId
  logInfoNSH "Got reverse geocode result: " humanReadableAddress'
  logInfoNSH "Got timezone result: " timezoneId'
  currTime <- liftIO getCurrentTime
  let masjidToCreate = Masjid name lat' lng' humanReadableAddress' timezoneId' currTime currTime
  logInfoNSH "Creating new masjid: " masjidToCreate
  doSql $ Just <$> insertRecord masjidToCreate

-- | Get salaah times for a masjid for a given day
getTimes :: Key Masjid -> Maybe UTCTime -> MyApp TimesResBody
getTimes masjidid maybeTime = case maybeTime of
  Nothing -> throwError err400 {errBody = "day parameter needs to be provided"}
  Just (UTCTime day _) -> do
    let (startOfDay, startOfNextDay) = getTodayAndTomorrow day
    times <- doSql $ selectList [SalaahTimeMasjidid ==. masjidid, SalaahTimeDatetime >=. startOfDay, SalaahTimeDatetime <. startOfNextDay] []
    TimesResBody <$> getAndPrependSunsetTime masjidid day (entityVal <$> times)

-- | Get sunset time for a masjid for a given day
getSunsetTimeForOneDay :: Key Masjid -- ^ Masjid id
  -> Day -- ^ Day to get sunset time for
  -> UTCTime -- ^ the current time. Used to construct a 'SalaahTime' object
  -> MyApp (Maybe SalaahTime) -- ^ The sunset time
getSunsetTimeForOneDay masjidid day currTime = do
  maybeMasjid <- doSql $ get masjidid
  case maybeMasjid of
    Nothing -> return Nothing
    Just (Masjid _ lat' lng' _ maybeTimezoneId _ _) -> case maybeTimezoneId of
      Nothing -> return Nothing
      Just timezoneId -> do
        sunsetTime <- liftIO $ getSunsetTime timezoneId (lat', lng') day
        return $ (\t -> SalaahTime Magrib masjidid t currTime currTime) <$> sunsetTime

getAndPrependSunsetTime :: Key Masjid -> Day -> [SalaahTime] -> MyApp [SalaahTime]
getAndPrependSunsetTime masjidid day ts = do
  currTime <- liftIO getCurrentTime
  maybeSalaahTime <- getSunsetTimeForOneDay masjidid day currTime
  case maybeSalaahTime of
    Nothing -> return ts
    Just t -> return (t:ts)

getAndPrependSunsetTimesForAMonth :: Key Masjid ->  Integer -> Int -> [SalaahTime] -> MyApp [SalaahTime]
getAndPrependSunsetTimesForAMonth masjidid year month ts = do
  currTime <- liftIO getCurrentTime
  let listOfDayObjs = yearAndMonthToListOfDays year month
  maybeSalaahTimes <- mapM (\d -> getSunsetTimeForOneDay masjidid d currTime) listOfDayObjs
  let salaahTimes = catMaybes maybeSalaahTimes
  return $ salaahTimes ++ ts

yearAndMonthToListOfDays :: Integer -> Int -> [Day]
yearAndMonthToListOfDays year month =
  fromGregorian year month <$> [1..monthLength (isLeapYear year) month]

-- | Get salaah times for a given masjid for today, where today
-- is what day the server thinks today is.
--
-- /This function is deprecated./
getTimesForToday :: Key Masjid -> MyApp TimesResBody
getTimesForToday masjidid = do
  currTime <- liftIO getCurrentTime
  getTimes masjidid (Just currTime)

-- | Get salaah times for a given masjid for a month
getTimesForAMonth :: Key Masjid -- ^ Masjid id
  -> Maybe Day -- ^ A day within the month to get salaah times for
  -> MyApp TimesResBody -- ^ The salaah times for a month
getTimesForAMonth masjidid maybeDay = case maybeDay of
  Nothing -> throwError err400 {errBody = "date parameter needs to be provided"}
  Just day -> do
    let (year, month, _) = toGregorian day
        startOfMonth = UTCTime (fromGregorian year month 1) 0
        (nextYear, nextMonth, _) = toGregorian $ addGregorianMonthsClip 1 day
        startOfNextMonth = UTCTime (fromGregorian nextYear nextMonth 1) 0
    times <- doSql $ selectList [SalaahTimeMasjidid ==. masjidid, SalaahTimeDatetime >=. startOfMonth, SalaahTimeDatetime <. startOfNextMonth] []
    TimesResBody <$> getAndPrependSunsetTimesForAMonth masjidid year month (entityVal <$> times)
