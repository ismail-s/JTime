module Middleware where

import qualified Data.Text as T
import Network.Wai (Middleware, pathInfo)

-- | Middleware that turns incoming requests with urls
-- with uppercase in the path into urls with only lowercase.
-- eg @Example.com\/Test -> Example.com\/test@
lowercaseUrlMiddleware :: Middleware
lowercaseUrlMiddleware app req = app newReq where
  oldUrl = pathInfo req
  newUrl = case oldUrl of
    url:rest -> url:(T.toLower <$> rest)
    _ -> oldUrl
  newReq = req {pathInfo = newUrl}
