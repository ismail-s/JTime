{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings, DataKinds, TypeOperators #-}
{-# LANGUAGE DeriveGeneric #-}

module Types where

import Servant
import Database
import Magicbane
import Data.Aeson.Types
import Data.HashMap.Lazy as HML
import Data.Text (Text)
import Data.Time.Calendar (Day)
import Data.Time.Clock (UTCTime(..))
import Database.Persist.Sqlite
import GHC.Generics
import Network.HTTP.Client (Manager)
import Logging (ModLogger)
import Settings (ApplicationSettings)
import Utils (utcTimeToString)
import Web.Internal.FormUrlEncoded (FromForm(..), parseUnique, unForm)

type MyAppContext = (ConnectionPool, Manager, ApplicationSettings, ModLogger)
type MyApp = MagicbaneApp MyAppContext

-- |Response body format for a single masjid, when getting a list of masjids
data GetMasjidsResBody = GetMasjidsResBody Int Text Location (Maybe Text) (Maybe Text) UTCTime UTCTime
  deriving Generic

instance ToJSON GetMasjidsResBody where
  toJSON (GetMasjidsResBody masjidid name loc humanReadableAddress timeZoneId createdAt lastModifiedAt) =
    object [ "id" .= masjidid
           , "name" .= name
           , "location" .= loc
           , "humanReadableAddress" .= humanReadableAddress
           , "timeZoneId" .= timeZoneId
           , "createdAt" .= createdAt
           , "lastModifiedAt" .= lastModifiedAt ]

data SalaahTimeWithJustTypeAndDatetime = SalaahTimeWithJustTypeAndDatetime SalaahType UTCTime
  deriving Generic

instance FromJSON SalaahTimeWithJustTypeAndDatetime where
  parseJSON = withObject "SalaahTimeWithJustTypeAndDatetime" $ \ v ->
    SalaahTimeWithJustTypeAndDatetime <$> v .: "type" <*> v .: "date"

data CreateOrUpdateMultipleReqBody = CreateOrUpdateMultipleReqBody (Key Masjid) [SalaahTimeWithJustTypeAndDatetime]
  deriving Generic

instance FromJSON CreateOrUpdateMultipleReqBody where
  parseJSON = withObject "CreateOrUpdateMultipleReqBody" $ \ v ->
    CreateOrUpdateMultipleReqBody <$> v .: "masjidId" <*> v .: "newOrUpdatedTimes"

data CreateOrUpdateReqBody = CreateOrUpdateReqBody (Key Masjid) SalaahType UTCTime
  deriving (Show, Generic)

instance FromJSON CreateOrUpdateReqBody where
  parseJSON = withObject "CreateOrUpdateReqBody" $ \ v ->
    CreateOrUpdateReqBody <$> v .: "masjidId" <*> v .: "type" <*> v .: "datetime"

instance FromForm (Maybe CreateOrUpdateReqBody) where
  fromForm f = if HML.null (unForm f) then Right Nothing
    else Just <$> (CreateOrUpdateReqBody <$>
                   parseUnique "masjidId" f <*>
                   parseUnique "type" f <*>
                   parseUnique "datetime" f)

data CreateMasjidReqBody = CreateMasjidReqBody
  {createMasjidName :: Text,
   createMasjidLocation :: Location} deriving (Show, Generic)

instance FromJSON CreateMasjidReqBody where
  parseJSON = withObject "CreateMasjidReqBody" $ \ v ->
    CreateMasjidReqBody <$> v .: "name" <*> v .: "location"

instance FromForm CreateMasjidReqBody where
  fromForm f =
    CreateMasjidReqBody <$> parseUnique "name" f <*> parseUnique "location" f

newtype TimesResBody = TimesResBody [SalaahTime]

instance ToJSON TimesResBody where
  toJSON (TimesResBody times) = object [ "times" .= times]

newtype CreateOrUpdateResBody = CreateOrUpdateResBody SalaahTime

instance ToJSON CreateOrUpdateResBody where
  toJSON (CreateOrUpdateResBody time) = object [ "instance" .= time]

newtype CreateOrUpdateMultipleResBody = CreateOrUpdateMultipleResBody [SalaahTime]

instance ToJSON CreateOrUpdateMultipleResBody where
  toJSON (CreateOrUpdateMultipleResBody times) = object [ "res" .= times]

data ExtendedSalaahTimeNearbyTimesResult = ExtendedSalaahTimeNearbyTimesResult
  {extendedSalaahTimeMasjidId :: Key Masjid,
   extendedSalaahTimeMasjidLocation :: Location,
   extendedSalaahTimeMasjidName :: Text,
   extendedSalaahTimeDatetime :: UTCTime,
   extendedSalaahTimeSalaahType :: SalaahType } deriving Generic

instance ToJSON ExtendedSalaahTimeNearbyTimesResult where
  toJSON (ExtendedSalaahTimeNearbyTimesResult masjidid loc masjidname datetime salaahType) =
    object [ "masjidId" .= masjidid
           , "masjidLocation" .= loc
           , "masjidName" .= masjidname
           , "datetime" .= utcTimeToString datetime
           , "type" .= salaahType ]

newtype TimesForMultipleMasjidsResBody = TimesForMultipleMasjidsResBody [ExtendedSalaahTimeNearbyTimesResult]

instance ToJSON TimesForMultipleMasjidsResBody where
  toJSON (TimesForMultipleMasjidsResBody times) = object [ "res" .= times]

data GetUserResBody = GetUserResBody
  {getUserId :: Int,
   getUserEmail :: Text} deriving (Show, Generic)

instance ToJSON GetUserResBody where
  toJSON (GetUserResBody userId email) = object [ "id" .= userId, "email" .= email]

data LoginToken = LoginToken Text Int

instance FromJSON LoginToken where
  parseJSON = withObject "LoginToken" $ \ v ->
    LoginToken <$> v .: "access_token"
               <*> v .: "userId"

instance ToJSON LoginToken where
  toJSON (LoginToken idToken userId) =
    object [ "access_token" .= idToken
           , "userId" .= userId ]

type MasjidRoutes = Get '[JSON] [GetMasjidsResBody]
    :<|> AuthProtect "token-auth" :> ReqBody '[FormUrlEncoded, JSON] CreateMasjidReqBody :> Post '[JSON] (Maybe Masjid)
    :<|> Capture "id" (Key Masjid) :> (
           "times" :> QueryParam "date" UTCTime :> Get '[JSON] TimesResBody
      :<|> "times-for-today" :> Get '[JSON] TimesResBody
      :<|> "times-for-a-month" :> QueryParam "date" Day :> Get '[JSON] TimesResBody)

type SalaahTimeRoutes = AuthProtect "token-auth" :> (
           "create-or-update" :> QueryParam "masjidId" (Key Masjid) :> QueryParam "type" SalaahType :> QueryParam "datetime" UTCTime :> ReqBody '[FormUrlEncoded, JSON] (Maybe CreateOrUpdateReqBody) :> Post '[JSON] CreateOrUpdateResBody
      :<|> "create-or-update-multiple" :>  ReqBody '[JSON] CreateOrUpdateMultipleReqBody :> Post '[JSON] CreateOrUpdateMultipleResBody)
    :<|>  QueryParam "location[lat]" Double :> QueryParam "location[lng]" Double :> QueryParam "location" Location :> QueryParam "salaahType" SalaahType :> QueryParam "searchRadius" Int :> QueryParams "faveMasjidIds" Int :> (
           "times-for-multiple-masjids" :> QueryParam "date" UTCTime :> Get '[JSON] TimesForMultipleMasjidsResBody
      :<|> "times-for-masjids-for-today" :> Get '[JSON] TimesForMultipleMasjidsResBody)

type UserRoutes = "googleid" :> QueryParam "id_token" Text :> Get '[JSON] LoginToken
    :<|> AuthProtect "token-auth" :> (
           Capture "userId" Int :> Get '[JSON] GetUserResBody
      :<|> "logout" :> PostNoContent '[FormUrlEncoded, JSON] NoContent)

type RestAPI = "api" :> (
       "masjids" :> MasjidRoutes
  :<|> "salaahtimes" :> SalaahTimeRoutes
  :<|> "user_tables" :> UserRoutes)

type FinalAPI = RestAPI :<|> Raw

finalAPI :: Proxy FinalAPI
finalAPI = Proxy
