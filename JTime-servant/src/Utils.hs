{-# LANGUAGE FlexibleContexts #-}
module Utils where

import Control.Monad.Error.Class (MonadError)
import qualified Data.ByteString.Lazy as BL
import Data.Time.Calendar (addDays, Day)
import Data.Time.Clock (UTCTime(..))
import Data.Time.Format (defaultTimeLocale, formatTime, iso8601DateFormat)
import Servant (throwError, err400, ServantErr(..))

eitherToMaybe :: Either e a -> Maybe a
eitherToMaybe (Left _) = Nothing
eitherToMaybe (Right a) = Just a

-- |Convert a time to a string of the form @%H:%M:%S.000Z@
utcTimeToString :: UTCTime -> String
utcTimeToString = formatTime defaultTimeLocale (iso8601DateFormat $ Just "%H:%M:%S.000Z")

throwErr400 :: MonadError ServantErr m => BL.ByteString -> m a
throwErr400 b = throwError err400 {errBody = b}

-- |Return times for the beginning of the provided day and the day after
getTodayAndTomorrow :: Day -> (UTCTime, UTCTime)
getTodayAndTomorrow day = (UTCTime day 0, UTCTime (addDays 1 day) 0)
