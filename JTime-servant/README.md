# JTime servant REST API

A REST API written in Haskell using the [Servant](https://haskell-servant.readthedocs.io) library.
This replaces the jtime-rest NodeJS REST API.
