# JTime [![build status](https://gitlab.com/ismail-s/JTime/badges/master/build.svg)](https://gitlab.com/ismail-s/JTime/commits/master)

See local masjid jamaat times on the move, with jamaat times submitted by anyone (via the website or Android app).

This project is composed of 3 parts:

- a REST API
- a [website](https://jtime.ismail-s.com)
- an [Android app](https://play.google.com/store/apps/details?id=com.ismail_s.jtime.android)

Contributions/bug reports/general feedback is welcome.